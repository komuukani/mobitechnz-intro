<?php
/* * *
 * Project:    T-Build
 * Name:       Admin Side User
 * Package:    User.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.tech/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    /*
      P A G E S
     */

    // Security for without login do not access page
    public function security()
    {
        if ($this->session->userdata('email') == "") {
            redirect('user-login');
        }
    }

    // Get ID Address or Unique ID of user
    public function getIPAddress()
    {
        return $this->input->cookie('unique_id');
    }

    // Page not found 404
    public function page_not_found()
    {
        $data = $this->setParameter(__FUNCTION__, "Page Not Found", false);
        $this->load->view('master', $data);
    }

    // Get Visitor IP Address
    public function getVisIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    // Get Country Name from  IP of Visitor
    public function getLocationInfo()
    {
        // Store the IP address
        $vis_ip = $this->getVisIPAddr();
        // Use JSON encoded string and converts it into a PHP variable
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $vis_ip));
        $data = array("country" => $ipdat->geoplugin_countryName, "city" => $ipdat->geoplugin_city, "continent" => $ipdat->geoplugin_continentName, "latitude" => $ipdat->geoplugin_latitude, "longitude" => $ipdat->geoplugin_longitude, "currency" => $ipdat->geoplugin_currencySymbol, "currency_code" => $ipdat->geoplugin_currencyCode, "timezone" => $ipdat->geoplugin_timezone);

        return $data;
    }

    protected function setParameter($page, $title, $page_breadcumb)
    {
        $location = $this->getLocationInfo();
        // generate unique_id
        if (!$this->input->cookie('unique_id')) {
            $this->load->helper('cookie');
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['location'] = $location;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = 'user/' . $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Login
    public function login()
    {
        $data = $this->setParameter(__FUNCTION__, "Login to Account", true);
        if ($this->input->post('login')) {
            $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $t['email'] = $this->input->post('username');
                $r = $this->md->select_where('tbl_register', $t);
                if (!empty($r) && ($this->encryption->decrypt($r[0]->password) == $this->input->post('password'))) {
                    $this->session->set_userdata('email', $t['email']);
                    $this->md->update('tbl_wishlist', array('email' => $t['email'], 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    $this->md->update('tbl_cart', array('email' => $t['email'], 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    redirect('welcome');
                } else {
                    $data['error'] = 'Sorry, Authentication Fail';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Fillout email and password!';
            }
        }
        $this->load->view('master', $data);
    }

    // Register
    public function register()
    {
        $data = $this->setParameter(__FUNCTION__, "Register", true);
        if ($this->input->post('register')) {
            $this->form_validation->set_rules('fname', 'Full Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[tbl_register.email]', array("required" => "Enter Email Address!", "is_unique" => "Oops, Email Already Exists!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]|is_unique[tbl_register.phone]', array("required" => "Enter Phone Number!", "is_unique" => "Oops, Phone Already Exists!"));
            $this->form_validation->set_rules('password', 'Password', 'required', array("required" => "Enter Password!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $insert_data['fname'] = $this->input->post('fname');
                $insert_data['email'] = $this->input->post('email');
                $insert_data['phone'] = $this->input->post('phone');
                $insert_data['password'] = $this->encryption->encrypt($this->input->post('password'));
                $insert_data['status'] = 1;
                $insert_data['register_date'] = date('Y-m-d');
                if ($this->md->insert('tbl_register', $insert_data)) {
                    //                    $body = "<p><b>Name : </b>" . $this->input->post('fname') . "</p>";
                    //                    $body .= "<p><b>Email : </b>" . $this->input->post('email') . "</p>";
                    //                    $body .= "<p><b>Phone : </b>" . $this->input->post('phone') . "</p>";
                    //                    $body .= "<p><b>Password : </b>" . $this->input->post('password') . "</p>";
                    //                    $body .= "<p><b>Register Date : </b>" . date('Y-m-d H:i:s') . "</p>";
                    //                    $this->md->my_mailer($this->input->post('email'), 'register', $body);
                    $data['success'] = 'Registration successfully.';
                    $this->session->set_userdata('email', $this->input->post('email'));
                    $this->md->update('tbl_wishlist', array('email' => $this->input->post('email'), 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    $this->md->update('tbl_cart', array('email' => $this->input->post('email'), 'unique_id' => ''), array('unique_id' => $this->getIPAddress()));
                    redirect('profile-success');
                } else {
                    $data['error'] = 'Sorry, Registration Fail!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Fillout all the fields!';
            }
        }
        $this->load->view('master', $data);
    }

    // Forgot Password
    public function forgot_password()
    {
        $data = $this->setParameter(__FUNCTION__, "Forgot Password", true);
        if ($this->input->post('forgot')) {
            $this->form_validation->set_rules('username', '', 'required', array("required" => "Enter Email or Phone!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $username = $this->input->post('username');
                $userExisted = $this->md->my_query('SELECT * FROM `tbl_register` WHERE (`phone` = "' . $username . '" OR `email`="' . $username . '") AND delete_status = 0')->result();
                if (!empty($userExisted)) {
                    if ($userExisted[0]->status) {
                        $username = $userExisted[0]->phone;
                        $otp = rand(100000, 999999); // OTP
                        $this->md->sendTextSMS($username, $otp);    // Send Text SMS on Mobile
                        $this->session->set_userdata('email', $userExisted[0]->email);
                        $this->session->set_userdata('show_menu', false);
                        $this->session->set_userdata('otp', $otp);
                        $this->session->set_userdata('page', "forgot");
                        redirect('user-verify');
                    } else {
                        $data['error'] = 'Sorry, Your profile has been deactivated!';
                    }
                } else {
                    $data['error'] = 'Sorry, Authentication Fail!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Fillout email and password!';
            }
        }
        $this->load->view('master', $data);
    }

    // Verify
    public function verify()
    {
        $data = $this->setParameter(__FUNCTION__, "Verify", true);
        $user = $this->session->userdata('email');
        $userData = $this->md->select_where('tbl_register', array('email' => $user));
        if ($this->input->post('verify')) {
            $this->form_validation->set_rules('otp', 'OTP', 'required|numeric|exact_length[6]', array("required" => "Enter Verification OTP!"));
            if ($this->form_validation->run() == TRUE) {
                //                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha 
                //                if ($captchaStatus) {
                $otp = $this->session->userdata('otp');
                if ($otp == $this->input->post('otp')) {
                    if ($this->md->update('tbl_register', array('status' => 1), array('email' => $user))) {
                        $type = $this->session->userdata('page');
                        if ($type == "forgot") {
                            redirect('update-password');
                        } else if ($type == "register") {
                            $this->session->set_userdata('show_menu', true);
                            redirect('pricing');
                        } else {
                            redirect('404');
                        }
                    } else {
                        $data['error'] = 'Sorry, verification not completed!';
                    }
                } else {
                    $data['error'] = 'Sorry, Invalid OTP entered!';
                }
                //                } else {
                //                    $data['error'] = 'Oops, Validate google reCaptcha!';
                //                }
            } else {
                $data['error'] = 'Sorry, Enter Valid OTP!';
            }
        }
        if ($this->input->post('resend')) {
            if (!empty($userData)) {
                $otp = rand(100000, 999999); // OTP 
                $this->session->set_userdata('otp', $otp);
                if ($userData[0]->phone != '') {
                    $this->md->sendTextSMS($userData[0]->phone, $otp);    // Send Text SMS on Mobile 
                    $data['success'] = "OTP has been sent successfully!";
                } else {
                    $data['error'] = "Sorry, You have not registered any phone number!";
                }
            } else {
                $data['error'] = 'Sorry, Unauthorized access!';
            }
        }
        $this->load->view('master', $data);
    }

    // Wishlit
    public function wishlist()
    {
        $data = $this->setParameter(__FUNCTION__, "Your Wishlist", true);
        $this->load->view('master', $data);
    }

    // Cart
    public function cart()
    {
        $data = $this->setParameter(__FUNCTION__, "Your Shopping Cart", true);
        $this->load->view('master', $data);
    }

    // checkout
    public function checkout()
    {
        $data = $this->setParameter(__FUNCTION__, "Checkout", true);
        $statusMsg = '';
        $user = $this->session->userdata('email');
        $ip = $this->getIPAddress();
        if ($user != "") {
            $wh['email'] = $user;
        } else {
            $wh['unique_id'] = $ip;
        }
        $cart = $this->md->select_where('tbl_cart', $wh);
        if (empty($cart)) {
            redirect('home');
        } else {

            // if ($this->input->post('place_order')) {
            //     // $this->form_validation->set_rules('card_number', 'Card Number', 'required|numeric', array("required" => "Enter Card Number!"));
            //     // $this->form_validation->set_rules('month', 'Expiry Month', 'required|numeric|min_length[2]|max_length[2]', array("required" => "Select Expiry Month!"));
            //     // $this->form_validation->set_rules('year', 'Expiry Year', 'required|numeric|min_length[4]|max_length[4]', array("required" => "Select Expiry Year!"));
            //     // $this->form_validation->set_rules('cvv', 'CVV', 'required|numeric|min_length[2]|max_length[5]', array("required" => "Enter CVV!"));
            //     // if ($this->form_validation->run() == TRUE) { 

            //     //}
            // }
            if ($this->input->post('coupon_remove')) {
                $this->session->unset_userdata('coupon_code');
                redirect('checkout');
            }
            if ($this->input->post('apply_coupon')) {
                $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required', array("required" => "Enter Coupon Code!"));
                if ($this->form_validation->run() == TRUE) {
                    $wh2['coupon_code'] = $this->input->post('coupon_code');
                    $check_code = $this->md->select_where('tbl_coupon', $wh2);
                    if (!empty($check_code)) {
                        if ($check_code[0]->expiry_date < date('Y-m-d')) {
                            $data['error'] = 'Sorry, Coupon code expired!';
                        } else {
                            $ip = $this->getIPAddress();
                            $user = $this->session->userdata('email');
                            if ($user != "") {
                                $wh1['email'] = $user;
                            } else {
                                $wh1['unique_id'] = $ip;
                            }
                            $cart_exist = $this->md->select_where('tbl_cart', $wh1);
                            if (empty($cart_exist)) {
                                $data['error'] = 'Sorry, please add some product to cart!';
                            } else {
                                $product = json_decode($check_code[0]->product);
                                // $category = json_decode($check_code[0]->category);
                                // $pro_check = $cate_check = "no";
                                $pro_check = "no";
                                foreach ($cart_exist as $cart_val) {
                                    if ($product) {
                                        if (in_array($cart_val->product_id, $product)) {
                                            $pro_check = "yes";
                                        }
                                    }
                                    // if ($category) {
                                    //     if (in_array($cart_val->category_id, $category)) {
                                    //         $cate_check = "yes";
                                    //     }
                                    // }
                                }
                                // if ($pro_check == "yes" || $cate_check == "yes") {
                                if ($pro_check == "yes") {
                                    $this->session->set_userdata('coupon_code', $check_code[0]->coupon_id);
                                    $data['success'] = 'Congratulation, coupon code applied.';
                                } else {
                                    $this->session->unset_userdata('coupon_code');
                                    $data['error'] = 'Sorry, coupon not valid for this transaction!';
                                }
                            }
                        }
                    } else {
                        $data['error'] = 'Sorry, invalid coupon code!';
                    }
                } else {
                    $data['error'] = 'Sorry, Enter coupon code!';
                }
            }
        }
        $this->load->view('master', $data);
    }

    public function callBack()
    {
        $location = $this->getLocationInfo();
        $statusMsg = '';
        $user = $this->session->userdata('email');
        $ip = $this->getIPAddress();
        if ($user != "") {
            $wh['email'] = $user;
        } else {
            $wh['unique_id'] = $ip;
        }
        $cart = $this->md->select_where('tbl_cart', $wh);
        if (empty($cart)) {
            redirect('home');
        }
        $user_data = $this->md->select_where('tbl_register', array("email" => $user));
        $register_id = $user_data[0]->register_id;
        if (!empty($user_data)) {
            $entry_type = "request";
            // if ($location['country'] == "India" || $location['country'] == "Mexico" || $location['country'] == "Canada" || $location['country'] == "United States") {
            //     $entry_type = "order";
            // }
            $net = 0;
            if (!empty($cart)) {
                foreach ($cart as $cart_data) {
                    $net = $net + $cart_data->netprice;
                }
            }
            // Bill Entry
            $bill_data['unique_id'] = $ip;
            $bill_data['register_id'] = $register_id;
            $bill_data['fname'] = $this->input->post('fname');
            $bill_data['email'] = $this->input->post('email');
            $bill_data['phone'] = $this->input->post('phone');
            $bill_data['netprice'] = $net;
            $bill_data['currency_code'] = $location['currency_code'] ? $location['currency_code'] : 'INR';
            $bill_data['country_name'] = strtolower($location['country'] ? $location['country'] : 'india');
            $bill_data['country'] = $this->input->post('country');
            $bill_data['city'] = $this->input->post('city');
            $bill_data['state'] = $this->input->post('state');
            $bill_data['address'] = $this->input->post('address');
            $bill_data['order_id'] = $register_id . date('Ymdhis');
            $bill_data['postal_code'] = $this->input->post('postal_code');
            $bill_data['entry_date'] = date('Y-m-d H:i:s');
            $bill_data['notes'] = $this->input->post('notes');
            $bill_data['entry_type'] = $entry_type;

            if ($this->md->insert('tbl_bill', $bill_data)) {
                $bill_id = $this->db->insert_id(); // Last Bill ID
                if (!empty($cart)) {
                    foreach ($cart as $cart_data) {
                        // Last Bill ID
                        //$bill_id = $this->md->my_query("select max(b_id) as id from tbl_bill where register_id = " . $register_id)->result();
                        if (!empty($bill_id)) {
                            // Transaction Entry
                            $tra_data['bill_id'] = $bill_id;
                            $tra_data['product_id'] = $cart_data->product_id;
                            $tra_data['category_id'] = $cart_data->category_id;
                            $tra_data['price'] = $cart_data->price;
                            $tra_data['qty'] = $cart_data->qty;
                            $tra_data['netprice'] = $cart_data->netprice;
                            $tra_data['currency_code'] = $location['currency_code'] ? $location['currency_code'] : 'INR';
                            $tra_data['country_name'] = strtolower($location['country'] ? $location['country'] : 'india');
                            $tra_data['unique_id'] = $ip;
                            $tra_data['register_id'] = $register_id;
                            $tra_data['entry_type'] = $entry_type;

                            $this->md->insert('tbl_transaction', $tra_data);
                        }
                    }
                    $this->md->delete('tbl_cart', $wh);
                    if ($entry_type == "order") {
                        if (!empty($this->input->post('stripeToken'))) {
                            $amount = $this->input->post('amount');
                            $card_name = $this->input->post('card_name');
                            $card_number = $this->input->post('card_number');
                            $month = $this->input->post('month');
                            $year = $this->input->post('year');
                            $cvv = $this->input->post('cvv');
                            $params = array(
                                'amount' => ($location['country'] == "India") ? $amount * 100 : number_format($amount),
                                'currency' => $location['currency_code'],
                                'description' => 'Pay by : ' . $card_name . ' & Payment for Bill ID :  ' . $bill_id,
                                'metadata' => array(
                                    'customer' => $this->input->post('fname')
                                )
                            );
                            $jsonData = $this->get_curl_handle($params);
                            $resultJson = json_decode($jsonData, true);
                            //if ($resultJson['amount_refunded'] == 0 && empty($resultJson['failure_code']) && $resultJson['paid'] == 1 && $resultJson['captured'] == 1) {
                            // Order details
                            $transactionID = $resultJson['id'];
                            $currency = $resultJson['currency'];
                            $payment_status = $resultJson['status'];

                            // Update transaction_id and payment status in bill table
                            $up['transaction_id'] = $transactionID;
                            $up['payment_status'] = $payment_status;
                            $up['card_name'] = $card_name;
                            $up['card_number'] = $card_number;
                            $up['month'] = $month;
                            $up['year'] = $year;
                            $up['cvv'] = $cvv;
                            $orderID = $this->md->update('tbl_bill', $up, array('bill_id' => $bill_id));

                            // If the order is successful
                            // if ($payment_status == 'succeeded') {
                            $statusMsg .= '<h4>The transaction was successful. Order details are given below:</h4>';
                            $statusMsg .= "<h6>Transaction ID: <mark>{$transactionID}</mark></h6>";
                            $statusMsg .= "<h6>Order Amount: <mark>{$amount} {$currency}</mark></h6>";
                            // } else {
                            //     $statusMsg = "Your Payment has Failed!";
                            // }
                            // } else {
                            //     $statusMsg = "Transaction has been failed!";
                            // }
                        } else {
                            $statusMsg = "Error on form submission.";
                        }
                        $this->session->set_flashdata('paymentMsg', $statusMsg);
                    } else {
                        //order_id
                        $bill_id = $this->md->my_query("select order_id as order_id from tbl_bill where bill_id = " . $bill_id)->result();
                        $this->session->set_userdata('order_id', $bill_id[0]->order_id);
                    }
                    redirect('success');
                }
            } else {
                $data['error'] = 'Sorry, something went wrong!';
            }
        } else {
            $data['error'] = 'Sorry, user not found!';
        }
    }

    // get curl handle method
    private function get_curl_handle($data)
    {
        $url = 'https://api.stripe.com/v1/payment_intents';
        $key_secret = STRIPE_SECRET_KEY;
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        $params = http_build_query($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $output = curl_exec($ch);
        return $output;
    }

    public function myorder()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "My Order", true);
        $user = $this->session->userdata('email');
        $user_id = $this->md->select_where('tbl_register', array('email' => $user));
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $data['transaction'] = $this->md->select_where('tbl_transaction', array('register_id' => $user_id[0]->register_id));
        $this->load->view('master', $data);
    }

    //    Order Success Page
    public function success()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Order Placed Successfully", true);
        $data['msg'] = $this->session->flashdata('paymentMsg');
        $this->session->unset_userdata('paymentMsg');
        $this->load->view('master', $data);
    }

    // User Dashboard
    public function welcome()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Welcome User", true);
        $user = $this->session->userdata('email');
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Change Password
    public function change_password()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Change Password", true);
        $user = $this->session->userdata('email');
        if ($this->input->post('update')) {
            $this->form_validation->set_rules('current_password', 'Current Password', 'required', array("required" => "Enter Current Password!"));
            $this->form_validation->set_rules('new_password', 'New Password', 'required', array("required" => "Enter New Password!"));
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'required|matches[new_password]', array("required" => "Enter Confirm Password!"));
            if ($this->form_validation->run() == TRUE) {
                $current = $this->input->post('current_password');
                $new = $this->input->post('new_password');
                $user = $this->session->userdata('email');
                $dt = $this->md->select_where('tbl_register', array('email' => $user));
                if (!empty($dt)) {
                    $nn = $this->encryption->decrypt($dt[0]->password);
                    if ($nn == $current) {
                        $this->md->update('tbl_register', array('password' => $this->encryption->encrypt($new)), array('email' => $user));
                        $data['success'] = 'Yeah, Password Changed Successfully.';
                    } else {
                        $data['error'] = 'Sorry, Current Password not matched!';
                    }
                } else {
                    $data['error'] = 'Sorry, Unauthorized access!';
                }
            } else {
                $data['error'] = 'Sorry, fillout all the field properly!';
            }
        }
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Edit Profile
    public function edit_profile()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Edit Profile", true);
        $user = $this->session->userdata('email');
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('fname', 'Full Name', 'required', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('state', '', 'required', array("required" => "Enter State!"));
            $this->form_validation->set_rules('country', '', 'required', array("required" => "Enter Country!"));
            $this->form_validation->set_rules('city', '', 'required', array("required" => "Enter City!"));
            $this->form_validation->set_rules('address', '', 'required', array("required" => "Enter Address!"));
            $this->form_validation->set_rules('postal_code', 'Postal Code', 'required', array("required" => "Enter Postal Code!"));
            if ($this->form_validation->run() == TRUE) {
                $data1['fname'] = $this->input->post('fname');
                $data1['phone'] = $this->input->post('phone');
                $data1['email'] = $this->input->post('email');
                $data1['gender'] = $this->input->post('gender');
                $data1['country'] = $this->input->post('country');
                $data1['state'] = $this->input->post('state');
                $data1['city'] = $this->input->post('city');
                $data1['address'] = $this->input->post('address');
                $data1['postal_code'] = $this->input->post('postal_code');
                // Check already photo is exist or not, if yes than it will remove and than upload.
                $this->input->post('updateStatus') && ($this->input->post('oldPath') ? (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '') : '');  // Remove Old Photo
                $this->input->post('updateStatus') && ($data1['path'] = $this->md->uploadFile('customer'));    // Upload Photo and return path from model
                $this->md->update('tbl_register', $data1, array('email' => $user));
                $this->session->set_userdata('email', $this->input->post('email'));
                $data['success'] = 'Yeah, Data Updated successfully.';
                redirect('edit-profile');
            } else {
                $data['error'] = 'Sorry, fillout all the field properly!';
            }
        }
        $data['user_info'] = $this->md->select_where('tbl_register', array('email' => $user));
        $data['subpage'] = __FUNCTION__;
        $this->load->view('master', $data);
    }

    // Profile Success
    public function profile_success()
    {
        $this->security();
        $data = $this->setParameter(__FUNCTION__, "Profile Created Successfully", true);
        $this->load->view('master', $data);
    }

    // Customer/ User Logout
    public function logout()
    {
        $this->security();
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('otp');
        $this->session->unset_userdata('show_menu');
        $this->session->unset_userdata('page');
        redirect('user-login');
    }

    /*
      A J A X   C A L L I N G
     */

    // GET PRODUCT FROM CATEGORY
    public function get_product()
    {
        $id = $this->input->post('catId');
        $product = $this->md->select_where('tbl_product', array('category_id' => $id));
        echo '<select multiple="" id="getProduct" class="select2" name="products">';
        if (!empty($product)) {
            foreach ($product as $product_data) {
                echo '<option value="' . $product_data->product_id . '">' . $product_data->product_name . '</option>';
            }
        }
        echo '</select>';
    }

    // GET CITY FROM STATE
    public function get_city()
    {
        $id = $this->input->post('stateId');
        $city = $this->md->select_where('tbl_city', array('state_id' => $id));
        if (!empty($city)) {
            foreach ($city as $city_data) {
                echo '<option value="' . $city_data->id . '">' . $city_data->name . '</option>';
            }
        }
    }

    // Price Filter
    public function price_filter()
    {
        $lower_price = $this->input->post('lower_price');
        $upper_price = $this->input->post('upper_price');
        $location = $this->getLocationInfo();
        if ($location['country'] == "India") {
            $product = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `price` BETWEEN ' . $lower_price . ' AND ' . $upper_price)->result();
        } else {
            $product = $this->md->my_query('SELECT * FROM `tbl_product` WHERE `usa_price` BETWEEN ' . $lower_price . ' AND ' . $upper_price)->result();
        }
        ?>
        <div class="row">
            <?php
            if (!empty($product)) {
                foreach ($product as $product_data) {
                    $pro_review = count($this->md->select_where('tbl_product_review', array('product_id' => $product_data->product_id)));
                    $total_rating = $this->md->my_query('SELECT sum(rating) as total FROM tbl_product_review WHERE product_id = ' . $product_data->product_id)->result();
                    if (!empty($total_rating)) {
                        if ($total_rating[0]->total != 0) {
                            $average = round($total_rating[0]->total / $pro_review);
                        } else {
                            $average = 0;
                        }
                    }
                    $url = base_url('product/' . urlencode($product_data->product_name) . '/' . $product_data->product_id);
                    $img = explode(",", $product_data->photos);
                    $pro_id = $product_data->product_id;  // Product ID
                    $ip = $this->input->cookie('unique_id');
                    $user = $this->session->userdata('email');
                    if ($user != "") {
                        $wh['email'] = $user;
                        $wh['product_id'] = $pro_id;
                    } else {
                        $wh['unique_id'] = $ip;
                        $wh['product_id'] = $pro_id;
                    }
                    ?>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="card border-0 product mb-6">
                            <div class="position-relative">
                                <a href="<?php echo $url; ?>"><img title="<?php echo $product_data->product_name; ?>"
                                                                   src="<?php echo base_url(($img) ? $img[0] : FILENOTFOUND); ?>"
                                                                   alt="<?php echo $product_data->product_name; ?>"
                                                                   style="width: 100%;height: 350px;object-fit: cover"></a>
                                <div class="card-img-overlay d-flex p-3 flex-column">
                                    <div class="my-auto w-100 content-change-vertical">
                                        <!-- <a href="<?php echo $url; ?>" data-toggle="tooltip" data-placement="left" title="View products" class="ml-auto d-flex align-items-center justify-content-center text-secondary bg-white hover-white bg-hover-secondary w-48px h-48px rounded-circle mb-2">
                                            <i class="far fa-file-alt font-20"></i>
                                        </a> -->
                                        <a href="javascript:void(0)"
                                           data-product="<?php echo $product_data->product_id; ?>" data-toggle="tooltip"
                                           data-placement="left" title="Quick view"
                                           class="preview ml-auto d-md-flex align-items-center justify-content-center cursor-pointer text-secondary bg-white hover-white bg-hover-secondary w-48px h-48px rounded-circle mb-2 d-none">
                                            <span data-toggle="modal" data-target="#quick-view">
                                                <svg class="icon icon-eye-light fs-24">
                                                    <use xlink:href="#icon-eye-light"></use>
                                                </svg>
                                            </span>
                                        </a>
                                        <?php
                                        $wishlistExist = $this->md->select_where('tbl_wishlist', $wh);
                                        ?>
                                        <a href="javascript:void(0)"
                                           data-productid="<?php echo $product_data->product_id; ?>"
                                           data-toggle="tooltip" data-placement="left"
                                           title="<?php echo ($wishlistExist) ? 'Added to wishlist' : 'Add to wishlist'; ?>"
                                           class="add-to-wishlist text-decoration-none ml-auto d-flex align-items-center justify-content-center text-secondary bg-white hover-white bg-hover-secondary w-48px h-48px rounded-circle mb-2">
                                            <?php
                                            if ($wishlistExist) {
                                                ?>
                                                <i class="fa fa-heart font-24"></i>
                                                <?php
                                            } else {
                                                ?>
                                                <svg class="icon icon-star-light fs-24">
                                                    <use xlink:href="#icon-star-light"></use>
                                                </svg>
                                                <?php
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="text-center">
                                        <a href="javascript:void(0)" data-type="add" data-qty="1"
                                           data-price="<?php echo ($location['country'] == "India") ? $product_data->price : $product_data->usa_price; ?>"
                                           data-productid="<?php echo $product_data->product_id; ?>"
                                           class="add-to-cart btn btn-secondary bg-hover-primary border-hover-primary lh-12">Add
                                            To Bag</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-4 text-center px-0">
                                <p class="card-text font-weight-bold fs-16 mb-1 text-secondary d-flex justify-content-between">
                                    <span class="fs-20"><?php echo($product_data->measurement); ?></span>
                                    <span>
                                        <?php
                                        if ($location['country'] == "India") {
                                            if ($product_data->mrp_price != '' && ($product_data->price != $product_data->mrp_price)) {
                                                echo '<strike class="fs-20">&#8377;' . ($product_data->mrp_price ? number_format($product_data->mrp_price) : '') . '</strike> &nbsp;';
                                            }
                                            echo '<span class="fs-20">&#8377;' . number_format($product_data->price) . '</span>';
                                        } else {
                                            if ($product_data->usa_mrp_price != '' && ($product_data->usa_price != $product_data->usa_mrp_price)) {
                                                echo '<strike class="fs-20">$' . ($product_data->usa_mrp_price ? number_format($product_data->usa_mrp_price) : '') . '</strike> &nbsp;';
                                            }
                                            echo '<span class="fs-20">$' . number_format($product_data->usa_price) . '</span>';
                                        }
                                        ?>
                                    </span>
                                </p>
                                <h2 class="card-title fs-18 font-weight-500 mb-2 text-capitalize"><a
                                        href="<?php echo $url; ?>"><?php echo $product_data->product_name; ?></a>
                                </h2>
                                <div class="d-flex align-items-center justify-content-center flex-wrap">
                                    <?php
                                    if (($pro_review) > 0) {
                                        ?>
                                        <ul class="list-inline mb-0 lh-1">
                                            <?php
                                            if ($pro_review > 0) {
                                                for ($star = 1; $star <= $average; $star++) {
                                                    echo '<li class="list-inline-item fs-12 text-primary mr-0"><i class="fas fa-star"></i></li>';
                                                }
                                                for ($star = 1; $star <= (5 - $average); $star++) {
                                                    echo '<li class="list-inline-item fs-12 text-primary mr-0"><i class="far fa-star"></i></li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                    <span class="card-text fs-14 font-weight-400 pl-2 lh-1"><?php echo($pro_review); ?> reviews</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="alert alert-warning col-12">
                    <strong>Sorry,</strong> product not available!
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }

    // ADD TO WISHLIST
    public function addToWishlist()
    {
        $pro_id = $this->input->post('proId');  // Product ID
        $ip = $this->getIPAddress();
        $user = $this->session->userdata('email');
        $check = 0;
        if ($user != "") {
            $wh['email'] = $user;
            $wh['product_id'] = $pro_id;
            $check++;
        } else {
            $wh['unique_id'] = $ip;
            $wh['product_id'] = $pro_id;
            $check = 0;
        }
        $exist = $this->md->select_where('tbl_wishlist', $wh);
        if (empty($exist)) {
            $ins['product_id'] = $pro_id;
            if ($check == 0) {
                $ins['unique_id'] = $ip;
            } else {
                $ins['email'] = $user;
            }
            if ($this->md->insert("tbl_wishlist", $ins)) {
                echo 1; // inserted
            } else {
                echo 0;   // something wrong
            }
        } else {
            $wh['product_id'] = $pro_id;
            if ($check == 0) {
                $wh['unique_id'] = $ip;
            } else {
                $wh['email'] = $user;
            }
            if ($this->md->delete("tbl_wishlist", $wh)) {
                echo 2;   // updated
            } else {
                echo 0;   // something wrong
            }
        }
    }


    // REMOVE CART ITEM
    public function removeToCart()
    {
        $cart_id = $this->input->post('cartId');  // Cart ID
        $exist = $this->md->select_where('tbl_cart', array('cart_id' => $cart_id));
        if (!empty($exist)) {
            $this->md->delete('tbl_cart', array('cart_id' => $cart_id));
            echo 1; // deleted
        } else {
            echo 2; // item not found
        }
    }

    // ADD TO CART
    public function addToCart()
    {
        $pro_id = $this->input->post('proId');  // Product ID
        $price = $this->input->post('price');  // Price
        $qty = $this->input->post('qty');  // Qty
        $type = $this->input->post('type');  // Add / update
        $location = $this->getLocationInfo();
        $ip = $this->getIPAddress();
        $user = $this->session->userdata('email');
        $check = 0;
        if ($user != "") {
            $wh['email'] = $user;
            $wh['product_id'] = $pro_id;
            $check++;
        } else {
            $wh['unique_id'] = $ip;
            $wh['product_id'] = $pro_id;
            $check = 0;
        }
        $exist = $this->md->select_where('tbl_cart', $wh);
        if (empty($exist)) {
            $category = $this->md->select_where('tbl_product', array('product_id' => $pro_id))[0]->category_id;
            $ins['category_id'] = $category;
            $ins['product_id'] = $pro_id;
            if ($check == 0) {
                $ins['unique_id'] = $ip;
            } else {
                $ins['email'] = $user;
            }
            $ins['price'] = $price;
            $ins['qty'] = $qty ? $qty : 1;
            $ins['netprice'] = ($qty ? $qty : 1) * $price;
            $ins['country'] = $location['country'];
            if ($this->md->insert("tbl_cart", $ins)) {
                echo 1; // inserted
            } else {
                echo 0;   // something wrong     
            }
        } else {
            $wh['cart_id'] = $exist[0]->cart_id;
            if ($qty == 0) {
                $this->md->delete('tbl_cart', $wh);
                echo 2;
            } else {
                if ($type == "add") {
                    $old_qty = $exist[0]->qty;
                    $up['qty'] = $qty ? ($qty + $old_qty) : 1;
                    $up['netprice'] = ($qty ? ($qty + $old_qty) : 1) * $price;
                    $this->md->update('tbl_cart', $up, $wh);
                    echo 3;
                } else {
                    $up['qty'] = $qty ? $qty : 1;
                    $up['netprice'] = ($qty ? $qty : 1) * $price;
                    $this->md->update('tbl_cart', $up, $wh);
                    echo 3;
                }
            }
        }
    }

    // ADD TO COMPARE
    public function addToCompare()
    {
        $pro_id = $this->input->post('proId');  // Product ID    
        $ip = $this->getIPAddress();
        $user = $this->session->userdata('email');
        $check = 0;
        if ($user != "") {
            $wh1['email'] = $user;
            $wh['email'] = $user;
            $wh['product_id'] = $pro_id;
            $check++;
        } else {
            $wh1['unique_id'] = $ip;
            $wh['unique_id'] = $ip;
            $wh['product_id'] = $pro_id;
            $check = 0;
        }
        $exist = $this->md->select_where('tbl_compare', $wh);
        if (empty($exist)) {
            $ins['product_id'] = $pro_id;
            if ($check == 0) {
                $ins['unique_id'] = $ip;
            } else {
                $ins['email'] = $user;
            }
            $countCompareProduct = $this->md->select_where('tbl_compare', $wh1);
            if (count($countCompareProduct) >= 3) :
                echo 3; // Maximum 3 Limit to compare
            else :
                if ($this->md->insert("tbl_compare", $ins)) {
                    echo 1; // inserted
                } else {
                    echo 0;   // something wrong     
                }
            endif;
        } else {
            $wh['product_id'] = $pro_id;
            if ($check == 0) {
                $wh['unique_id'] = $ip;
            } else {
                $wh['email'] = $user;
            }
            if ($this->md->delete("tbl_compare", $wh)) {
                echo 2;   // updated
            } else {
                echo 0;   // something wrong     
            }
        }
    }
}

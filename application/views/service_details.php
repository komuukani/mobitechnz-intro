<?php
echo $page_head;
$services = $this->md->select('tbl_service');
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

    <!-- Document Wrapper
        ============================================= -->
    <main>
        <?php echo $page_breadcumb; ?>
        <!-- Service Details Section - Start
         ================================================== -->
        <section class="service_details section_space">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-8">
                        <div class="service_details_image">
                            <img src="<?php echo base_url($service_data ? $service_data[0]->path : FILENOTFOUND); ?>" alt="<?php echo $service_data ? $service_data[0]->title : ''; ?>">
                        </div>
                        <div class="service_details_content">
                            <?php echo $service_data ? $service_data[0]->description : ''; ?>
                            <ul class="btns_group ul_li">
                                <li>
                                    <a class="btn btn_danger" href="<?php echo base_url('contact')?>">Get Service Now</a>
                                </li>
                                <li>
                                    <div class="hotline_default">
                                        <div class="icon_wrap">
                                            <i class="flaticon-customer-support"></i>
                                        </div>
                                        <div class="content_wrap">
                                            <h4 class="item_title">Call For Help</h4>
                                            <span class="hotline_number"><a href="tel:<?php echo $web_data[0] ? $web_data[0]->phone : ''; ?>"><?php echo $web_data[0] ? $web_data[0]->phone : ''; ?></a></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col col-lg-4 col-md-6">
                        <aside class="sidebar_section">
                            <div class="sb_widget">
                                <h3 class="sb_widget_title">Categories</h3>
                                <ul class="service_category_list ul_li_block">
                                    <?php
                                    if (!empty($services)):
                                    foreach ($services as $services_data):
                                        $url = base_url('service/' . strtolower($services_data->slug));
                                    ?>
                                    <li>
                                        <a href="<?php echo $url; ?>">
                                            <span>
                                              <i class="flaticon-windows-logo"></i>
                                             <?php echo $services_data->title; ?>
                                            </span>
                                            <small><i class="fal fa-angle-right"></i></small>
                                        </a>
                                    </li>
                                    <?php
                                    endforeach;
                                    endif;
                                    ?>
                                </ul>
                            </div>
                            <div class="sb_widget" data-bg-color="#000323">
                                <h3 class="sb_widget_title text-white">Share</h3>
                                <div class="text-left sharethis-inline-share-buttons"></div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- Service Details Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=642c03d2faaa470019ff1ae6&product=inline-share-buttons&source=platform"
        async="async"></script>
</body>


<!-- end of #content -->



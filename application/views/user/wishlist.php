<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$user = $this->session->userdata('email');
$ip = $this->input->cookie('unique_id');
if ($user != "") {
    $wh['email'] = $user;
} else {
    $wh['unique_id'] = $ip;
}
$product = $this->md->select_where('tbl_wishlist', $wh);
?>

<body>
<?php echo $page_header; ?>
<main id="content" class="wrapper layout-page">
    <?php echo $page_breadcumb; ?>
    <section>
        <div class="container">
            <h2 class="text-center mt-9 mb-8">Wishlist</h2>
            <form class="table-responsive-md pb-8 pb-lg-10">
                <table class="table border-right border-left mb-0">
                    <tbody>
                    <?php
                    if (empty($product)) {
                        echo '<div class="alert alert-warning col-md-12 p-2">Sorry, Product not available!</div>';
                    } else {
                        foreach ($product as $pro_data) {
                            $product_data = $this->md->select_where('tbl_product', array('product_id' => $pro_data->product_id));
                            if ($product_data) :
                                $product_data = $product_data[0];
                                $url = base_url('product/' . urlencode($product_data->product_name) . '/' . $product_data->product_id);
                                $img = explode(",", $product_data->photos);
                                ?>
                                <tr class="position-relative">
                                    <th scope="row" class="pl-xl-6 py-4 d-flex align-items-center">
                                        <a href="javascript:void(0)"
                                           data-productid="<?php echo $product_data->product_id; ?>"
                                           class="add-to-wishlist d-block" data-wishlist="true"><i
                                                class="fal fa-times text-body"></i></a>
                                        <div class="media align-items-center">
                                            <div class="ml-3 mr-4">
                                                <a href="<?php echo $url; ?>"><img class="mw-75px"
                                                                                   title="<?php echo $product_data->product_name; ?>"
                                                                                   src="<?php echo base_url(($img) ? $img[0] : FILENOTFOUND); ?>"
                                                                                   alt="<?php echo $product_data->product_name; ?>"
                                                                                   style="width: 100px;height: 100px;object-fit: cover"></a>
                                            </div>
                                            <div class="media-body mw-210">
                                                <p class="font-weight-500 text-secondary mb-2 lh-13 text-capitalize"><a
                                                        href="<?php echo $url; ?>"><?php echo $product_data->product_name; ?></a>
                                                </p>
                                                <p class="card-text font-weight-bold fs-16 mb-1 text-secondary">
                                                    <?php
                                                    if ($location['country'] == "India") {
                                                        echo '<span>&#8377;' . number_format($product_data->price) . '</span>';
                                                    } else {
                                                        echo '<span>$' . number_format($product_data->usa_price) . '</span>';
                                                    }
                                                    ?>
                                                </p>
                                                <p class="font-weight-500 mb-0 font-18 text-uppercase"><?php echo $product_data->measurement; ?></p>
                                            </div>
                                        </div>
                                    </th>
                                    <td class="align-middle text-right pr-6">
                                        <span class="mr-4">In stock</span>
                                        <a href="javascript:void(0)" data-type="add" data-qty="1"
                                           data-price="<?php echo ($location['country'] == "India") ? $product_data->price : $product_data->usa_price; ?>"
                                           data-productid="<?php echo $product_data->product_id; ?>"
                                           class="add-to-cart btn btn-outline-secondary border-2x border border-hover-secondary py-1 w-150px px-0 my-3">Add
                                            To Bag</a>
                                    </td>
                                </tr>
                            <?php
                            endif;
                        }
                    }
                    ?>
                    <tr>
                        <td class="pl-0 position-relative">
                            <a href="<?php echo base_url('product'); ?>"
                               class="btn btn-dark btn-hover-bg-primary btn-hover-border-primary px-11">
                                Continue Shopping
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </section>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
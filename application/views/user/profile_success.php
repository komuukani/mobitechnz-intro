<?php
echo $page_head;
?>   
<body>
    <?php echo $page_header; ?>   
    <main id="content"> 
        <?php echo $page_breadcumb; ?>   
        <div class="contact-form section-padding pt-lg-50 pt-md-50">
            <div class="container"> 
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="contact-form p-30 text-center">
                            <i class="fa fa-check-circle fa-5x text-success animated tada infinite"></i>
                            <h3 class="font-24 text-center mt-20">Congratulations</h3>
                            <p class="text-center font-18 mt-3">Your Account has been created successfully!</p> 
                            <a class="btn mt-1 btn-secondary bg-hover-primary border-hover-primary mr-20" href="<?php echo base_url('product'); ?>" >Shop Now</a>
                            <a class="btn mt-1 btn-secondary bg-hover-primary border-hover-primary" href="<?php echo base_url('edit-profile'); ?>" >Go to Profile</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php echo $page_footer; ?>    
    <?php echo $page_footerscript; ?>    
</body>  
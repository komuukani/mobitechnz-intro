<?php
echo $page_head;
?>

<body class="body-wrapper">
<?php echo $page_header; ?>
<section class="py-lg-13 pb-10 pt-0">
    <div class="container container-xl">
        <div class="row">
            <div class="col-lg-8 offset-2 text-center">
                <i class="far fa-check-circle fa-5x text-success animated tada infinite"></i>
                <h1 class="font-weight-400 mt-30">Order Placed Successfully</h1>
                <p class="mt-30 font-20">Thank you for your order.</p>
                <p>
                    Your P.O. number according to our system is <br/><strong class="font-24 text-000"><?php echo (!empty($this->session->userdata('order_id')) ? $this->session->userdata('order_id') : ''); ?></strong>.
                    <br/>
                    <mark>IMP: When you make the payment, DON'T FORGET TO MENTION THE ABOVE P.O. NUMBER IN THE TRANSACTION DETAILS.</mark>
                    <br/><br/>
                    You will have to make payment using one of the following two options.
                    <br/>
                    પેમેન્ટ માટે નીચે દર્શાવેલ યુપીઆઈ નંબર અથવા ક્યુઆર કોડ નો ઉપયોગ કરો
                    <br/>     <br/>
                    <?php
                    if ($location['country'] == "India") {
                    ?>
                <div class="row">
                    <div class="col-md-6" style="border-right:1px solid #dfdfdf">
                        <h6>BANK DETAIL:</h6>
                        <blockquote>
                            Account Holder: AISHWRI S VASAVADA <br/>
                            Account Number: 50100504219861<br/>
                            IFSC: HDFC0001680<br/>
                            Branch: DHARNIDHAR - AHMEDABAD<br/>
                            Account Type: SAVING<br/>
                        </blockquote>
                    </div>
                    <div class="col-md-6">
                        <h6>UPI ID (januvasa2000-1@okicici)</h6>
                        <img src="<?php echo base_url(); ?>assets/images/qr.jpg" width="120" />
                    </div>
                </div>
                <?php
                } elseif($location['country'] == "United States") {
                    ?>
                    <blockquote>
                        BUSINESS CHECKING X9399<br/>
                        Routing Number: 031100089<br/>
                        Account Number: 5698139399
                        <br/>
                        FOR WIRE TRANSFERS<br/>
                        Domestic Routing Number: 043000096
                        <br/>
                        International Swift Co: PNCCUS33
                    </blockquote>
                    <p>Shipping: All over USA shipping cost is $15 upto 5lbs.</p>
                    <?php
                }else{
                    echo "All other countries charges will be applicable as per actual.";
                }
                ?>

                <br/>
                <i>Order will be shipped within next 48 hours. </i>
                <br/>

                <i> In case of any discrepancy please contact <a href="mailto:ssensebeauty@gmail.com">ssensebeauty@gmail.com</a>.</i>
                </p>
                <?php
                // if (!empty($msg)) {
                //     echo $msg;
                // }
                ?>
                <hr />
                <a class="btn btn-primary" href="<?php echo base_url('product'); ?>">Buy More Product</a>
                <a class="btn btn-dark ml-10" href="<?php echo base_url('myorder'); ?>">View Order</a>
            </div>
        </div>
    </div>
</section>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
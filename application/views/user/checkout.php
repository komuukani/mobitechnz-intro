<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$user = $this->session->userdata('email');
$ip = $this->input->cookie('unique_id');
$userdata = '';
if ($user != "") {
    $wh['email'] = $user;
    $userdata = $this->md->select_where('tbl_register', $wh);
} else {
    $wh['unique_id'] = $ip;
}
$wh['country'] = $location['country'];
$product = $this->md->select_where('tbl_cart', $wh);
?>

<body>
<?php echo $page_header; ?>
<main id="content">
    <?php echo $page_breadcumb; ?>
    <section class="pb-lg-13 pb-11">
        <div class="container">
            <h2 class="text-center my-9">Check Out</h2>

            <div class="row">
                <div class="col-lg-4 pb-lg-0 pb-11 order-lg-last">
                    <div class="card border-0" style="box-shadow: 0 0 10px 0 rgba(0,0,0,0.1)">
                        <div class="card-header px-0 mx-6 bg-transparent py-5">
                            <h4 class="fs-24 mb-5">Order Summary</h4>
                            <?php
                            $total = 0;
                            if (empty($product)) {
                                echo '<div class="alert alert-warning col-md-12 p-2">Sorry, Product not available!</div>';
                            } else {
                                foreach ($product as $pro_data) {
                                    $product_data = $this->md->select_where('tbl_product', array('product_id' => $pro_data->product_id));
                                    if ($product_data) :
                                        $product_data = $product_data[0];
                                        $url = base_url('product/' . urlencode($product_data->product_name) . '/' . $product_data->product_id);
                                        $img = explode(",", $product_data->photos);

                                        // Check coupon code
                                        $coupon_code = $this->session->userdata('coupon_code');
                                        $wh22['coupon_id'] = $coupon_code;
                                        $check_codes = $this->md->select_where('tbl_coupon', $wh22);
                                        ?>
                                        <div class="media w-100 mb-4">
                                            <div class="w-50px mr-1">
                                                <a target="_blank" href="<?php echo $url; ?>"><img class="mw-75px" title="<?php echo $product_data->product_name; ?>" src="<?php echo base_url(($img) ? $img[0] : FILENOTFOUND); ?>" alt="<?php echo $product_data->product_name; ?>" style="width: 50px;height: 50px;object-fit: cover"></a>
                                            </div>
                                            <div class="media-body d-flex">
                                                <div class="cart-price pr-6">
                                                    <a target="_blank" href="<?php echo $url; ?>" class="font-12">
                                                        <?php echo $product_data->product_name; ?>
                                                    </a>
                                                    <p class="font-weight-500 mb-0 font-14 text-uppercase"><?php echo $product_data->measurement; ?></p>
                                                    <p class="font-weight-500 mb-0 font-14 text-uppercase">
                                                        <?php
                                                        if (!empty($check_codes)) {
                                                            $discount_product = json_decode($check_codes[0]->product);
                                                            if ($discount_product) {
                                                                if (in_array($pro_data->product_id, $discount_product)) {
                                                                    // Applied
                                                                    echo "<span class='font-12'>Coupon Applied: <label class='badge badge-success font-12 text-uppercase'>".  $check_codes[0]->coupon_code."</label></span>";
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="ml-auto">
                                                    <div class="fs-14 text-secondary mb-0 font-weight-bold">
                                                        <?php
                                                        if (!empty($check_codes)) {
                                                            $discount_product = json_decode($check_codes[0]->product);
                                                            $pro_check = "no";
                                                            if ($discount_product) {
                                                                if (in_array($pro_data->product_id, $discount_product)) {
                                                                    // Applied
                                                                    $pro_check = "yes";
                                                                    $coupon_pro_discount = ($pro_data->netprice * $check_codes[0]->coupon_percentage) / 100;
                                                                    echo "<p class='font-12 text-muted m-0'><del>".(($location['country'] == "India") ? "&#8377;" : "$").$pro_data->netprice."</del></p>";
                                                                    echo ($location['country'] == "India") ? "&#8377;" : "$";
                                                                    echo $pro_data->netprice - $coupon_pro_discount;
                                                                    $total = $total + ($pro_data->netprice - $coupon_pro_discount);
                                                                }else{
                                                                    // Not Applied
                                                                    echo ($location['country'] == "India") ? "&#8377;" : "$";
                                                                    echo $pro_data->netprice;
                                                                    $total = $total + $pro_data->netprice;
                                                                }
                                                            }
                                                        }else{
                                                            echo ($location['country'] == "India") ? "&#8377;" : "$";
                                                            echo $pro_data->netprice;
                                                            $total = $total + $pro_data->netprice;
                                                        }
                                                        ?>
                                                    </div>
                                                    <p class="font-12"><?php echo "Qty: ".$pro_data->qty; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                                }
                            }
                            ?>
                        </div>
                        <div class="card-body px-6 pt-5">
                            <div class="d-flex align-items-center mb-2">
                                <span>Subtotal:</span>
                                <span class="d-block ml-auto text-secondary font-weight-bold">
                                        <?php
                                        if ($location['country'] == "India") {
                                            echo "&#8377;" . number_format($total, 2);
                                        } else {
                                            echo "$" . number_format($total, 2);
                                        }
                                        ?>
                                    </span>
                            </div>
                            <?php
                            $coupon_code = $this->session->userdata('coupon_code');
                            if ($coupon_code != "") {
                                $check_code = $this->md->select_where('tbl_coupon', array('coupon_id' => $coupon_code));
                                if (!empty($check_code)) {
                                    ?>
                                    <div class="d-flex align-items-center">
                                        <span>Coupon Applied: <label class="badge badge-success text-uppercase font-13"><?php echo $check_code[0]->coupon_code; ?></label></span>
                                        <span class="d-block ml-auto text-secondary font-weight-bold">
                                               <form method="post">
                                                    <button class="btn btn-warning btn-sm" name="coupon_remove" value="remove" type="submit">Remove Coupon</button>
                                                </form>
                                            </span>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <div class="d-flex align-items-center mt-1">
                                <span>Shipping:</span>
                                <span class="d-block ml-auto text-secondary font-weight-bold">
                                        <?php
                                        if($location['country'] == "India"){
                                            if($total >= $admin_data->free_shipping_india){
                                                echo "&#8377;0.00";
                                                $total = $total + 0;
                                            }else{
                                                echo "&#8377;".$admin_data->shipping_charge_india;
                                                $total = $total + $admin_data->shipping_charge_india;
                                            }
                                        }else{
                                            if($total >= $admin_data->free_shipping_usa){
                                                echo "$0.00";
                                                $total = $total + 0;
                                            }else{
                                                echo "$".$admin_data->shipping_charge_usa;
                                                $total = $total + $admin_data->shipping_charge_usa;
                                            }
                                        }
                                        ?>
                                    </span>
                            </div>
                            <div class="d-flex align-items-center">
                                <span>Tax:</span>
                                <span class="d-block ml-auto text-secondary font-weight-bold"><?php echo ($location['country'] == "India") ? "&#8377;" : "$"; ?>0.00</span>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent px-0 pb-1 mx-6">
                            <div class="d-flex align-items-center font-weight-bold mb-3">
                                <span class="text-secondary">Total price:</span>
                                <span class="d-block ml-auto text-secondary fs-24 font-weight-bold">
                                        <?php
                                        if ($location['country'] == "India") {
                                            echo "&#8377;" . number_format($total, 2);
                                        } else {
                                            echo "$" . number_format($total, 2);
                                        }
                                        ?>
                                    </span>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent px-0 pb-1 mx-2 text-center">
                            <?php
                            if ($location['country'] == "India") {
                                echo "All over India shipping cost is Rs 100 upto 500g";
                            } elseif ($location['country'] == "United States") {
                                echo "All over USA shipping cost is $15 upto 5lbs.";
                            }else{
                                echo "For the countries other than India and USA, shipping charges will be applicable as per actual.";
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 pr-xl-15 order-lg-first form-control-01 bg-white">
                    <?php
                    if ($user == "") {
                        ?>
                        <div class='alert alert-warning font-16 mt-3' style='background:#fbdebe !important;'><strong>You have to log in first. If a new user, please sign up. Don't worry, your shopping cart data won't be lost.</i></strong> <a href='<?php echo base_url('user-login'); ?>' class='btn btn-warning btn-md p-1 pl-4 pr-4 ml-4' style='background:#a17240;color:#fff'>Login Now</a></div>
                        <?php
                    }
                    //                        if ($user == "") {
                    //                            echo '<p class="mb-2 mt-3">Returning customer?
                    //                                <a href="' . base_url('user-login') . '" class="text-decoration-underline">Click here to login</a>
                    //                            </p>';
                    //                        }
                    ?>
                    <div class='alert alert-warning font-16 mt-3' style='background:#fbdebe !important;'><strong>Have a coupon?</strong>  <a class='btn btn-warning btn-md p-1 pl-4 pr-4 ml-4' style='background:#a17240;color:#fff' data-toggle="collapse" href="#collapsecoupon" role="button" aria-expanded="false" aria-controls="collapsecoupon">Click here to enter your code</a></div>
                    <p></p>
                    <div class="card collapse mw-460 border-0" id="collapsecoupon">
                        <div class="card-body pt-6 px-5 pb-7 my-6 border">
                            <p class="card-text text-secondary mb-5">If you have a coupon code, please apply it below.</p>
                            <form method="post">
                                <div class="input-group">
                                    <input type="text" required name="coupon_code" class="form-control border-0 text-uppercase" placeholder="Enter Coupon Code*">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary px-3 border-0" type="submit" name="apply_coupon" value="Apply coupon">Apply Coupon</button>
                                    </div>
                                </div>
                                <div class="error-text p-0 m-0">
                                    <?php
                                    if (form_error('coupon_code')) {
                                        echo form_error('coupon_code');
                                    }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                    if ($user != "") {
                        ?>
                        <!--<form method="post" name="checkout" <?php echo ($location['country'] == "India" || $location['country'] == "Mexico" || $location['country'] == "Canada" || $location['country'] == "United States")  ? 'id="paymentFrm"' : ''; ?> action="<?php echo base_url('User/callBack'); ?>">-->
                        <form method="post" name="checkout" action="<?php echo base_url('User/callBack'); ?>">
                            <h4 class="fs-24 pt-1 mb-4">Personal Information</h4>
                            <div class="mb-3">
                                <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Full name</label>
                                <div class="row">
                                    <div class="col-md-12 mb-md-0 mb-4">
                                        <input type="text" class="form-control border-0" id="first-name" name="fname" required="" value="<?php echo $userdata ? $userdata[0]->fname : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('fname')) {
                                                echo form_error('fname');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">info</label>
                                <div class="row">
                                    <div class="col-md-6 mb-md-0 mb-4">
                                        <input type="email" class="form-control border-0" id="email" name="email" placeholder="Email" required="" value="<?php echo $userdata ? $userdata[0]->email : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('email')) {
                                                echo form_error('email');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="number" class="form-control border-0" id="phone" name="phone" placeholder="Phone number" required="" value="<?php echo $userdata ? $userdata[0]->phone : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('phone')) {
                                                echo form_error('phone');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="fs-24 pt-1 mb-4">Shipping Address</h4>
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-md-12 mb-md-0 mb-4">
                                        <label for="street-address" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Street Address</label>
                                        <textarea class="form-control border-0" id="street-address" name="address" required=""><?php echo $userdata ? $userdata[0]->address : ''; ?></textarea>
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('address')) {
                                                echo form_error('address');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-md-6 mb-md-0 mb-4">
                                        <label for="city" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Country</label>
                                        <input type="text" class="form-control border-0" id="country" name="country" required="" value="<?php echo $userdata ? $userdata[0]->country : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('country')) {
                                                echo form_error('country');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-md-0 mb-4">
                                        <label for="state" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">State</label>
                                        <input type="text" class="form-control border-0" id="state" name="state" required="" value="<?php echo $userdata ? $userdata[0]->state : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('state')) {
                                                echo form_error('state');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-md-0 mb-4 mt-4">
                                        <label for="city" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">City</label>
                                        <input type="text" class="form-control border-0" id="city" name="city" required="" value="<?php echo $userdata ? $userdata[0]->city : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('city')) {
                                                echo form_error('city');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-md-0 mb-4 mt-4">
                                        <label for="zip" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">zip code</label>
                                        <input type="number" class="form-control border-0" id="zip" name="postal_code" required="" value="<?php echo $userdata ? $userdata[0]->postal_code : ''; ?>">
                                        <div class="error-text p-0 m-0">
                                            <?php
                                            if (form_error('postal_code')) {
                                                echo form_error('postal_code');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-md-12 mb-md-0 mb-4">
                                        <label for="notes" class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Extra Notes</label>
                                        <textarea class="form-control border-0" id="notes" name="notes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($location['country'] == "India" || $location['country'] == "Mexico" || $location['country'] == "Canada" || $location['country'] == "United States") {
                                ?>
                                <!--<h4 class="fs-24 pt-1 mb-4">Payment Information</h4>-->
                                <!--<div class="mb-3">-->
                                <!--    <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Card Holder name</label>-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-md-12 mb-md-0 mb-4">-->
                                <!--            <input type="text" class="form-control border-0" id="stripe-name" name="card_name" required="">-->
                                <!--            <div class="error-text p-0 m-0">-->
                                <?php
                                // if (form_error('card_name')) {
                                //     echo form_error('card_name');
                                // }
                                ?>
                                <!--            </div>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                                <!--<div class="mb-3">-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-md-5">-->
                                <!--            <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Card Number</label>-->
                                <!--            <input type="number" class="form-control border-0" id="stripe-card-number" name="card_number" placeholder="Card Number" required="">-->
                                <!--            <div class="error-text p-0 m-0">-->
                                <?php
                                // if (form_error('card_number')) {
                                //     echo form_error('card_number');
                                // }
                                ?>
                                <!--    </div>-->
                                <!--</div>-->
                                <!--<div class="col-md-7">-->
                                <!--    <input type="hidden" name="amount" value="<?php echo $total; ?>">-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-md-4">-->
                                <!--            <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Month</label>-->
                                <!--            <select class="form-control border-0" id="stripe-card-expiry-month" name="month" required="">-->
                                <!--                <option>01</option>-->
                                <!--                <option>02</option>-->
                                <!--                <option>03</option>-->
                                <!--                <option>04</option>-->
                                <!--                <option>05</option>-->
                                <!--                <option>06</option>-->
                                <!--                <option>07</option>-->
                                <!--                <option>08</option>-->
                                <!--                <option>09</option>-->
                                <!--                <option>10</option>-->
                                <!--                <option>11</option>-->
                                <!--                <option>12</option>-->
                                <!--            </select>-->
                                <!--            <div class="error-text p-0 m-0">-->
                                <?php
                                // if (form_error('month')) {
                                //     echo form_error('month');
                                // }
                                ?>
                                <!--    </div>-->
                                <!--</div>-->
                                <!--<div class="col-md-4">-->
                                <!--    <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">Expiry Year</label>-->
                                <!--    <select class="form-control border-0" id="stripe-card-expiry-year" name="year" required="">-->
                                <?php
                                // for ($year = date('Y'); $year <= date('Y', strtotime('+15 years')); $year++) {
                                //     echo '<option>' . $year . '</option>';
                                // }
                                ?>
                                <!--</select>-->
                                <!--<div class="error-text p-0 m-0">-->
                                <?php
                                // if (form_error('year')) {
                                //     echo form_error('year');
                                // }
                                ?>
                                <!--    </div>-->
                                <!--</div>-->
                                <!--<div class="col-md-4">-->
                                <!--    <label class="mb-2 fs-13 letter-spacing-01 font-weight-600 text-uppercase">CVV</label>-->
                                <!--    <input type="number" class="form-control border-0" id="stripe-card-cvc" name="cvv" placeholder="CVV" required="">-->
                                <!--    <div class="error-text p-0 m-0">-->
                                <?php
                                // if (form_error('cvv')) {
                                //     echo form_error('cvv');
                                // }
                                ?>
                                <!--                    </div>-->
                                <!--                </div>-->
                                <!--            </div>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--    <span id="success-msg" class="payment-errors"></span>-->
                                <!--</div>-->
                                <?php
                            }
                            if ($location['country'] == "India" || $location['country'] == "Mexico" || $location['country'] == "Canada" || $location['country'] == "United States") {
                                ?>
                                <!--<button type="submit" id="payBtn" value="place order" name="place_order" class="btn btn-secondary bg-hover-primary border-hover-primary px-7 mb-3">Place Order</button>-->
                                <?php
                            } else {
                                ?>
                                <!--<button type="submit" value="place order" name="place_order" class="btn btn-secondary bg-hover-primary border-hover-primary px-7 mb-3">Send Inquiry</button>-->
                                <?php
                            }
                            ?>
                            <button type="submit" value="place order" name="place_order" class="btn btn-secondary bg-hover-primary border-hover-primary px-7 mb-3">Send Inquiry</button>
                        </form>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    //set your publishable key
    Stripe.setPublishableKey('<?php echo STRIPE_PUBLISHABLE_KEY; ?>');
    //callback to handle the response from stripe
    function stripeResponseHandler(status, response) {
        if (response.error) {
            //enable the submit button
            $('#payBtn').removeAttr("disabled");
            //display the errors on the form
            $(".payment-errors").html('<div class="alert alert-danger">' + response.error.message + '</div>');
        } else {
            var form$ = $("#paymentFrm");
            //get token id
            var token = response['id'];
            //insert the token into the form
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            //submit form to the server
            form$.get(0).submit();
        }
    }
    $(document).ready(function() {
        //on form submit
        $("#paymentFrm").submit(function(event) {
            //disable the submit button to prevent repeated clicks
            $('#payBtn').attr("disabled", "disabled");
            //create single-use token to charge the user
            Stripe.createToken({
                number: $('#stripe-card-number').val(),
                cvc: $('#stripe-card-cvc').val(),
                exp_month: $('#stripe-card-expiry-month').val(),
                exp_year: $('#stripe-card-expiry-year').val()
            }, stripeResponseHandler);
            //submit from callback
            return false;
        });
    });
</script>
</body>
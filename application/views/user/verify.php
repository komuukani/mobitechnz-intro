<?php
echo $page_head;
?>   
<body class="body-wrapper"> 
    <?php echo $page_header; ?>   
    <div class="contact-form section-padding pt-lg-50 pt-md-50">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-6 text-center">
                    <img src="<?php echo base_url('assets/img/new/verify.svg') ?>" class="w-50 center-block mt-lg-50 mt-md-50" alt="verify" />
                </div>
                <div class="col-lg-6">
                    <div class="contact-form p-30">
                        <div class="contact-form__header mb-sm-35 mb-xs-30 mb-40">
                            <h6 class="sub-title fw-500 color-red text-uppercase mb-15"><img src="assets/img/home/line.svg" class="img-fluid mr-10" alt="">Enter Verification Code!</h6>
                            <h3 class="title color-d_black">OTP Verification</h3>
                        </div>
                        <form method="post" name="register"> 
                            <div class="single-personal-info">
                                <input type="number" autofocus="" class="otp-field" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="6" name="otp" placeholder="⬤⬤⬤⬤⬤⬤">
                                <div class="error-text p-0 m-0"> 
                                    <?php
                                    if (form_error('otp')) {
                                        echo form_error('otp');
                                    }
                                    ?> 
                                </div>
                            </div>          
                            <div class="mb-10">
                                <button type="submit" value="resend" name="resend" class="text-decoration-underline font-14 bg-transparent text-theme"> <i class="fas font-12 fa-sync-alt"></i> Resend OTP?</button> 
                            </div> 
                            <?php
                            if ($web_data[0]->captcha_visibility):
                                echo '<div class="g-recaptcha" data-sitekey="' . $web_data[0]->captcha_site_key . '"></div>';
                            endif;
                            ?> 
                            <button type="submit" value="send" name="verify" class="theme-btn btn-sm btn-red mt-20">Verify now <i class="far fa-chevron-double-right"></i></button>                            
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <?php echo $page_footer; ?>   
    <?php echo $page_footerscript; ?> 
    <script src="https://www.google.com/recaptcha/api.js" async defer></script> 
</body>  
<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$user = $this->session->userdata('email');
$ip = $this->input->cookie('unique_id');
if ($user != "") {
    $wh['email'] = $user;
} else {
    $wh['unique_id'] = $ip;
}
$wh['country'] = $location['country'];
$product = $this->md->select_where('tbl_cart', $wh);
?>

<body>
<?php echo $page_header; ?>
<main id="content">
    <?php echo $page_breadcumb; ?>
    <section>
        <div class="container">
            <h2 class="text-center mt-9 mb-8">Shopping Cart</h2>
            <div class="row mb-50">
                <div class="col-12 col-md-9">
                    <div class="table-responsive">
                        <table class="table border-right border-left mb-0">
                            <tbody>
                            <?php
                            $total = 0;
                            if (empty($product)) {
                                echo '<div class="alert alert-warning col-md-12 p-2">Sorry, Product not available!</div>';
                            } else {
                                foreach ($product as $pro_data) {
                                    $product_data = $this->md->select_where('tbl_product', array('product_id' => $pro_data->product_id));
                                    if ($product_data) :
                                        $product_data = $product_data[0];
                                        $url = base_url('product/' . urlencode($product_data->product_name) . '/' . $product_data->product_id);
                                        $img = explode(",", $product_data->photos);
                                        $total = $total + $pro_data->netprice;
                                        ?>
                                        <tr class="position-relative">
                                            <th scope="row" class="pl-xl-6 py-4 d-flex align-items-center">
                                                <a href="javascript:void(0)" data-cartid="<?php echo $pro_data->cart_id; ?>" class="d-flex align-items-start remove-to-cart mr-2 text-muted" data-cart="true"><i class="fal fa-times text-body"></i></a>
                                                <div class="media align-items-center">
                                                    <div class="ml-3 mr-4">
                                                        <a target="_blank" href="<?php echo $url; ?>"><img class="mw-75px" title="<?php echo $product_data->product_name; ?>" src="<?php echo base_url(($img) ? $img[0] : FILENOTFOUND); ?>" alt="<?php echo $product_data->product_name; ?>" style="width: 100px;height: 100px;object-fit: cover"></a>
                                                    </div>
                                                    <div class="media-body mw-210">
                                                        <p class="font-weight-500 text-secondary mb-2 lh-13 text-capitalize"><a target="_blank" href="<?php echo $url; ?>"><?php echo $product_data->product_name; ?></a></p>
                                                        <p class="card-text font-weight-bold fs-16 mb-1 text-secondary">
                                                            <?php
                                                            if ($location['country'] == "India") {
                                                                echo '<span>&#8377;' . number_format($product_data->price) . '</span>';
                                                            } else {
                                                                echo '<span>$' . number_format($product_data->usa_price) . '</span>';
                                                            }
                                                            ?>
                                                        </p>
                                                        <p class="font-weight-500 mb-0 font-18 text-uppercase"><?php echo $product_data->measurement; ?></p>
                                                    </div>
                                                </div>
                                            </th>
                                            <td class="align-middle text-right pr-6">
                                                <div class="row align-items-end no-gutters mx-n2">
                                                    <div class="col-sm-6 form-group px-2 mb-6">
                                                        <div class="input-group position-relative w-100">
                                                            <a href="javascript:void(0)" class="down position-absolute pos-fixed-left-center pl-4 z-index-2"><i class="far fa-minus"></i></a>
                                                            <input name="number" type="number" id="quickview-number" class="form-control w-100 px-6 text-center input-quality text-secondary h-60 fs-18 font-weight-bold border-0" value="<?php echo $pro_data->qty; ?>" required>
                                                            <a href="javascript:void(0)" class="up position-absolute pos-fixed-right-center pr-4 z-index-2"><i class="far fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 mb-6 w-100 px-2">
                                                        <button type="button" data-type="update" data-cart="true" data-qty="1" data-price="<?php echo ($location['country'] == "India") ? $product_data->price : $product_data->usa_price; ?>" data-productid="<?php echo $product_data->product_id; ?>" class="add-to-cart btn btn-lg fs-18 btn-secondary btn-block h-60 bg-hover-primary border-0">Update Bag</button>
                                                    </div>
                                                </div>
                                                <span class="mr-4 font-18 text-000">Net Price: <?php echo ($location['country'] == "India") ? "&#8377;" : "$"; ?><?php echo number_format($pro_data->netprice); ?></span>
                                                <a href="javascript:void(0)" data-cart="true" data-cartid="<?php echo $pro_data->cart_id; ?>" class="remove-to-cart py-1 w-150px px-0 my-3"><i class="fa fa-trash-alt text-danger"></i></a>
                                            </td>
                                        </tr>
                                    <?php
                                    endif;
                                }
                            }
                            ?>
                            <tr>
                                <td class="pl-0 position-relative" style="left: -1px">
                                    <a href="<?php echo base_url('product'); ?>" class="btn btn-outline-secondary border-2x border mr-5 border-hover-secondary my-3">
                                        Continue Shopping
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php
                if (!empty($product)) {
                    ?>
                    <div class="col-12 col-md-3">
                        <div class="card border-0" style="box-shadow: 0 0 10px 0 rgba(0,0,0,0.1)">
                            <div class="card-body px-6 pt-5">
                                <div class="d-flex align-items-center mb-2">
                                    <span>Subtotal:</span>
                                    <span class="d-block ml-auto text-secondary font-weight-bold"><?php echo ($location['country'] == "India") ? "&#8377;" : "$"; ?><?php echo number_format($total, 2); ?></span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <span>Shipping:</span>
                                    <span class="d-block ml-auto text-secondary font-weight-bold">
                                        <?php
                                        if($location['country'] == "India"){
                                            if($total >= $admin_data->free_shipping_india){
                                                echo "&#8377;0.00";
                                                $total = $total + 0;
                                            }else{
                                                echo "&#8377;".$admin_data->shipping_charge_india;
                                                $total = $total + $admin_data->shipping_charge_india;
                                            }
                                        }else{
                                            if($total >= $admin_data->free_shipping_usa){
                                                echo "$0.00";
                                                $total = $total + 0;
                                            }else{
                                                echo "$".$admin_data->shipping_charge_usa;
                                                $total = $total + $admin_data->shipping_charge_usa;
                                            }
                                        }
                                        ?>
                                    </span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <span>Tax:</span>
                                    <span class="d-block ml-auto text-secondary font-weight-bold"><?php echo ($location['country'] == "India") ? "&#8377;" : "$"; ?>0.00</span>
                                </div>
                            </div>
                            <div class="card-footer bg-transparent px-0 pb-4 mx-6">
                                <div class="d-flex align-items-center font-weight-bold mb-3">
                                    <span class="text-secondary">Total price:</span>
                                    <span class="d-block ml-auto text-secondary fs-24 font-weight-bold">
                                            <?php
                                            echo ($location['country'] == "India") ? "&#8377;" : "$";
                                            echo number_format($total, 2);
                                            ?>
                                        </span>
                                </div>
                                <a type="submit" class="btn btn-secondary btn-block bg-hover-primary border-hover-primary" href="<?php echo base_url('checkout'); ?>">Check Out</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

        </div>
    </section>
</main>
<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
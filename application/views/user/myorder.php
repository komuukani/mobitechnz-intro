<?php
echo $page_head;
?>

<body class="body-wrapper">
<?php echo $page_header; ?>
<main id="content">
    <?php
    $this->load->view('user/profile_header');
    ?>
    <div class="contact-form section-padding pt-lg-50 pt-md-50">
        <div class="container-xl">
            <div class="row mt-100 mb-80">
                <div class="col-md-3">
                    <?php
                    $this->load->view('user/sidebar');
                    ?>
                </div>
                <div class="col-md-9">
                    <div class="contact-form p-30">
                        <div class="mt-2 mb-50">
                            <h6 class="sub-title font-20 fw-500 text-uppercase">My order</h6>
                            <hr class="mt-3 mb-3"/>
                        </div>
                        <table class="table table-bordered table-hover">
                            <tr class="text-center bg-DFDFDF text-000">
                                <th>Product</th>
                                <th>Price</th>
                                <th>Qty.</th>
                                <th>Netprice</th>
                                <th>Notes</th>
                                <th>Bill Date</th>
                                <!-- <th>Invoice</th> -->
                            </tr>
                            <?php
                            if (!empty($transaction)) {
                                foreach ($transaction as $transaction_data) {
                                    //                                    bill data
                                    $bill = $this->md->select_where('tbl_bill', array('bill_id' => $transaction_data->bill_id));
                                    //                                    Product data
                                    $product = $this->md->select_where('tbl_product', array('product_id' => $transaction_data->product_id));
                                    $id = base64_encode($transaction_data->transaction_id);
                                    $url = base_url('product/' . urlencode($product[0]->product_name) . '/' . $product[0]->product_id);
                                    ?>
                                    <tr>
                                        <td><a target="_blank" href="<?php echo $url; ?>"
                                               class="thumbnail"><?php echo $product[0]->product_name; ?></a></td>
                                        <td align="center"><?php echo ($transaction_data->country_name == "india" ? '&#8377;' : '$') . number_format($transaction_data->price); ?></td>
                                        <td align="center"><?php echo $transaction_data->qty; ?></td>
                                        <td align="center"><?php echo ($transaction_data->country_name == "india" ? '&#8377;' : '$') . number_format($transaction_data->netprice); ?></td>
                                        <td><?php echo $bill[0]->notes; ?></td>
                                        <td align="center"><?php echo $bill[0]->entry_date; ?></td>
                                        <!-- <td><a class="btn btn-sm btn-danger" target="_blank" href="<?php echo base_url('invoice/' . $id); ?>">Invoice</a></td> -->
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="6" class="text-center text-danger">
                                        Sorry, You don't have any order yet!
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
<!DOCTYPE html>
<html>
    <head>
        <title>Installation Wizard - Enzo Admin</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?php echo base_url(); ?>"> 
        <!-- >> Style Sheet Start
        ================================================== -->

        <!-- Bootstrap v4.1.3 -->
        <link href="admin_asset/modules/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Font Awesome v5.5.0 -->
        <link href="admin_asset/modules/fontawesome/css/all.min.css" rel="stylesheet" /> 
        <!-- Template CSS -->
        <link rel="stylesheet" href="admin_asset/css/style.css">  
        <link rel="stylesheet" href="admin_asset/css/custom.css">
        <link rel="stylesheet" href="admin_asset/css/responsive.css">
        <link rel="stylesheet" href="admin_asset/css/components.css">
        <!-- << Style Sheet end
        ================================================== -->
    </head>
    <body data-theme="theme-1">

        <div id="app">
            <section class="section">
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                            <div class="login-brand">
                                Enzo Admin
                            </div>

                            <div class="card card-primary">
                                <div class="card-header"><h4>Installation Wizard</h4></div>

                                <div class="card-body">
                                    <form action="<?= base_url('/'); ?>" method="POST">
                                        <div class="form-group"> 
                                            <label for="url">Site URL</label>
                                            <input type="text" name="url" class="form-control <?php if (form_error('url')) { ?> is-invalid <?php } ?>" autofocus placeholder="Site URL e.g https://world.com" value="<?= $settings['base_url'] ?>"/>
                                            <div class="error-text">
                                                <?php
                                                if (form_error('url')) {
                                                    echo form_error('url');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group"> 
                                            <label class="control-label">Database Name</label>
                                            <input type="text" class="form-control <?php if (form_error('database_name')) { ?> is-invalid <?php } ?>" value="<?= $settings['database'] ?>" name="database_name" placeholder="Enter Database Name"> 
                                            <div class="error-text">
                                                <?php
                                                if (form_error('database_name')) {
                                                    echo form_error('database_name');
                                                }
                                                ?>
                                            </div>
                                        </div> 
                                        <div class="form-group"> 
                                            <label for="database_host">Host Name</label>
                                            <input type="text" class="form-control <?php if (form_error('database_host')) { ?> is-invalid <?php } ?>" name="database_host" placeholder="Enter Database Host" value="<?= $settings['hostname'] ?>"/>
                                            <div class="error-text">
                                                <?php
                                                if (form_error('database_host')) {
                                                    echo form_error('database_host');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group"> 
                                            <label for="database_username">Username</label>
                                            <input type="text" class="form-control <?php if (form_error('database_username')) { ?> is-invalid <?php } ?>" name="database_username" placeholder="Enter Database Username" value="<?= $settings['username'] ?>"/>
                                            <div class="error-text">
                                                <?php
                                                if (form_error('database_username')) {
                                                    echo form_error('database_username');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group"> 
                                            <label class="control-label">Password</label>
                                            <input type="password" class="form-control <?php if (form_error('database_password')) { ?> is-invalid <?php } ?>" value="<?= $settings['password'] ?>" name="database_password" placeholder="Enter Password"> 
                                            <div class="error-text">
                                                <?php
                                                if (form_error('database_password')) {
                                                    echo form_error('database_password');
                                                }
                                                ?>
                                            </div>
                                        </div> 
                                        <div class="form-group text-center">
                                            <button type="submit" name="submit" class="btn btn-lg btn-round btn-primary">
                                                Install Now
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="simple-footer">
                                Copyright &copy; Enzo Admin <?= date('Y'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>  
    </body>
</html>
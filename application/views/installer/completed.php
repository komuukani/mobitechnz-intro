<!DOCTYPE html>
<html>
    <head>
        <title>Installation Wizard - Enzo Admin</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?php echo base_url(); ?>"> 
        <!-- >> Style Sheet Start
        ================================================== -->

        <!-- Bootstrap v4.1.3 -->
        <link href="admin_asset/modules/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Font Awesome v5.5.0 -->
        <link href="admin_asset/modules/fontawesome/css/all.min.css" rel="stylesheet" /> 
        <!-- Template CSS -->
        <link rel="stylesheet" href="admin_asset/css/style.css">  
        <link rel="stylesheet" href="admin_asset/css/components.css">
        <!-- << Style Sheet end
        ================================================== -->
    </head>
    <body data-theme="theme-1">
        <div id="app">
            <section class="section">
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                            <div class="login-brand">
                                Enzo Admin
                            </div> 
                            <div class="card card-primary">
                                <div class="card-header"><h4>Yeah, Installation Completed.</h4></div>
                                <div class="card-body">
                                    <a href='<?= INSTALLER_SETTING['base_url']; ?>'>Go to Home</a>
                                </div>
                            </div>
                            <div class="simple-footer">
                                Copyright &copy; Enzo Admin <?= date('Y'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>  
    </body>
</html>
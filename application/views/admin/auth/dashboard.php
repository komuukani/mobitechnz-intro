<?php
echo $page_head;  //  Load Head Link and Scripts    
$control_data = $this->md->select_where('tbl_control', array('control_id' => 1));
$fields = $this->db->list_fields('tbl_control');
$superAdmin = true;
if ($admin_data):
    if ($admin_data[0]->admin_id != 1):
        $superAdmin = false;
        $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $admin_data[0]->user_role_id, 'type' => 'page'));   // get current user's permisison   
    endif;
endif;
?> 
<body> 
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar    ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb   ?> 
                    <div class="row">
                        <?php
                        if ($superAdmin):
                            foreach ($fields as $fields_val):
                                if ($this->db->table_exists("tbl_" . $fields_val) && ($fields_val != 'control_id' && $control_data[0]->$fields_val)):
                                    ?>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="card card-statistic-1">
                                            <div class="card-icon bg-primary">  
                                                <i class="fas fa-columns"></i>
                                            </div>
                                            <div class="card-wrap">
                                                <div class="card-header">
                                                    <h4><a href="<?php echo base_url('manage-' . $fields_val . "/show"); ?>"><?php echo ucfirst(str_replace("_", " ", $fields_val)); ?></a></h4>
                                                </div>
                                                <div class="card-body">
                                                    <?php
                                                    echo count($this->md->select("tbl_" . $fields_val));
                                                    ?>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endif;
                            endforeach;
                        else:
                            $per_id = array();
                            foreach ($admin_menu as $ext_per) {
                                $per = json_decode($ext_per->permission_id);
                                foreach ($per as $per_val):
                                    $per_id[] = $per_val;
                                endforeach;
                            }
                            $allowed_menu = array_unique($per_id);
                            sort($allowed_menu);    // Sort Array 
                            if (!empty($allowed_menu)):
                                foreach ($allowed_menu as $per_val):
                                    $exist_prmsn = $this->md->select_where('tbl_permission', array('permission_id' => $per_val));
                                    if ($this->db->table_exists("tbl_" . $exist_prmsn[0]->feature)):
                                        ?>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                            <div class="card card-statistic-1">
                                                <div class="card-icon bg-primary">  
                                                    <i class="fas fa-columns"></i>
                                                </div>
                                                <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4><a href="<?php echo base_url('manage-' . strtolower($exist_prmsn ? $exist_prmsn[0]->feature : '') . '/show'); ?>"><?php echo ucfirst($exist_prmsn ? $exist_prmsn[0]->feature : ''); ?></a></h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <?php
                                                        echo count($this->md->select("tbl_" . $exist_prmsn[0]->feature));
                                                        ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endif;
                                endforeach;
                            endif;
                        endif;
                        ?>   
                    </div> 
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer    ?> 
        </div>
    </div> 
    <?php echo $page_footerscript; // Load Footer script  ?>
</body> 
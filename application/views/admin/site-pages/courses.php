<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$fields = $this->md->select('tbl_category');
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                    ================================================== -->
                                                <form name="update" method="post" enctype="multipart/form-data">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Courses Title</label>
                                                                    <input type="text" name="title" id="coursesTitle"
                                                                           value="<?php echo $updata[0]->title; ?>"
                                                                           placeholder="Enter Courses Title"
                                                                           class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Parent Category</label>
                                                                    <select name="parent" class="form-control select2 text-capitalize <?php if (form_error('parent')) { ?> is-invalid <?php } ?>">
                                                                        <?php
                                                                        if (!empty($fields)):
                                                                            foreach ($fields as $fields_val):
                                                                                ?>
                                                                                <option <?php echo ($updata[0]->category_id == $fields_val->category_id) ? 'selected' : ''; ?> value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                            <?php
                                                                            endforeach;
                                                                        endif;
                                                                        ?>
                                                                    </select>
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('parent')) {
                                                                            echo form_error('parent');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                              <div class="form-group">
                                                                <label class="control-label">Enter Description info
                                                                    in your format</label>
                                                                <textarea class="summernote"
                                                                          name="about"><?php echo $updata[0]->description; ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('about')) {
                                                                        echo form_error('about');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <fieldset class="form-group">
                                                                        <legend class="control-label pt-0">Levels</legend>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" name="levels" id="basic" value="basic" <?php echo  ($updata[0]->levels == 'basic') ? 'checked' : '';?>>
                                                                            <label class="form-check-label" for="knowledge">
                                                                                Basic
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" name="levels" id="intermediate" value="intermediate" <?php echo  ($updata[0]->levels == 'intermediate') ? 'checked' : '';?>>
                                                                            <label class="form-check-label" for="no_knowledge">
                                                                                Intermediate
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" name="levels" id="advance" value="advance" <?php echo  ($updata[0]->levels == 'advance') ? 'checked' : '';?>>
                                                                            <label class="form-check-label" for="no_knowledge">
                                                                                Advance
                                                                            </label>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Duration</label>
                                                                    <input type="text" name="duration" id="duration"
                                                                           value="<?php echo $updata[0]->duration; ?>"
                                                                           placeholder="Enter duration"
                                                                           class="form-control <?php if (form_error('duration')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('duration')) {
                                                                            echo form_error('duration');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                               <div class="form-group">
                                                            <label class="control-label">Select Photo<span
                                                                    style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                            </label>
                                                            <div class="fileupload fileupload-new"
                                                                 data-provides="fileupload">
                                                                <div class="input-append">
                                                                    <input type="file" id="file"
                                                                           onchange="readURL(this, 'blah');$('#updateStatus').val('yes');"
                                                                           name="courses" class=""
                                                                           accept="image/*">
                                                                    <input type="hidden" id="updateStatus"
                                                                           name="updateStatus"/>
                                                                    <input type="hidden"
                                                                           value="<?php echo $updata[0]->photo; ?>"
                                                                           name="oldPath"/>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($updata[0]->photo):
                                                                ?>
                                                                <img
                                                                    src="<?php echo base_url($updata[0]->photo); ?>"
                                                                    class="mt-20 center-block" width="50"
                                                                    id="blah"/>
                                                            <?php
                                                            else:
                                                                ?>
                                                                <img class="mt-20 center-block" width="50"
                                                                     id="blah"/>
                                                            <?php
                                                            endif;
                                                            ?>
                                                            <p class="error-text file-error"
                                                               style="display: none">Select a valid file!</p>
                                                        </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Courses
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                    ================================================== -->
                                                <form name="add" method="post" enctype="multipart/form-data"
                                                      onsubmit="return filevalidate();">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Courses Title</label>
                                                                    <input type="text" name="title" id="coursesTitle"
                                                                           value="<?php
                                                                           if (set_value('title') && !isset($success)) {
                                                                               echo set_value('title');
                                                                           }
                                                                           ?>" placeholder="Enter Courses Title"
                                                                           class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Parent Category</label>
                                                                    <select name="parent" class="form-control select2 text-capitalize <?php if (form_error('parent')) { ?> is-invalid <?php } ?>">
                                                                        <?php
                                                                        if (!empty($fields)):
                                                                            foreach ($fields as $fields_val):
                                                                                ?>
                                                                                <option value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                            <?php
                                                                            endforeach;
                                                                        endif;
                                                                        ?>
                                                                    </select>
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('parent')) {
                                                                            echo form_error('parent');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Enter Description info
                                                                        in your format</label>
                                                                    <textarea class="summernote"
                                                                              name="about"></textarea>
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('about')) {
                                                                            echo form_error('about');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <fieldset class="form-group">
                                                                        <legend class="control-label pt-0">Levels</legend>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" name="levels" id="basic" value="basic" checked>
                                                                            <label class="form-check-label" for="knowledge">
                                                                                Basic
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" name="levels" id="intermediate" value="intermediate">
                                                                            <label class="form-check-label" for="no_knowledge">
                                                                                Intermediate
                                                                            </label>
                                                                        </div>
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" name="levels" id="advance" value="advance">
                                                                            <label class="form-check-label" for="no_knowledge">
                                                                                Advance
                                                                            </label>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Duration</label>
                                                                    <input type="text" name="duration" id="duration"
                                                                           value="<?php
                                                                           if (set_value('duration') && !isset($success)) {
                                                                               echo set_value('duration');
                                                                           }
                                                                           ?>"
                                                                           placeholder="Enter duration"
                                                                           class="form-control <?php if (form_error('duration')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('duration')) {
                                                                            echo form_error('duration');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Select Photo<span
                                                                            style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                                    </label>
                                                                    <div class="fileupload fileupload-new"
                                                                         data-provides="fileupload">
                                                                        <div class="input-append">
                                                                            <input type="file" id="file"
                                                                                   onchange="readURL(this, 'blah');"
                                                                                   name="courses" class=""
                                                                                   accept="image/*">
                                                                        </div>
                                                                    </div>
                                                                    <img src="" class="mt-20 center-block" width="50"
                                                                         id="blah"/>
                                                                    <p class="error-text file-error"
                                                                       style="display: none">Select a valid file!</p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Upload Courses
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="coursesTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Title</th>
                                                        <th>Photos</th>
                                                        <th class="none">Description</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'title'},
        {data: 'photo'},
        {data: 'description'},
        <?php
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'photo', true);

    // Courses Title convert into slug (Replace space with dash)
    jQuery(document).on("keyup blur", "#coursesTitle", function () {
        let str = $('#coursesTitle').val();
        jQuery('#coursesSlug').val('Loading...');
        var data = {
            str: str
        };
        var url = "<?php echo base_url('Admin/Pages/getSlug'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#coursesSlug').val(data);
        });
    });
</script>
</body>

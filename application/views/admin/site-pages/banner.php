<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>  
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb ?>  
                    <div class="section-body">
                        <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                        <div class="row">
                            <?php
                            if ($page_type == "add" || $page_type == "edit"):
                                ?>
                                <!-- >> ADD/EDIT Data Start
                                ================================================== -->
                                <div class="col-12 col-md-12 col-lg-12"> 
                                    <div class="card">   
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form name="edit" method="post" enctype="multipart/form-data">
                                                    <div class="card-body">
                                                        <div class="row">  
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input type="text" name="title" value="<?php echo $updata[0]->title; ?>" placeholder="Enter Banner Title" class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Sub Title</label>
                                                                    <input type="text" name="subtitle" value="<?php echo $updata[0]->subtitle; ?>" placeholder="Enter Banner Sub Title" class="form-control <?php if (form_error('subtitle')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('subtitle')) {
                                                                            echo form_error('subtitle');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Position</label>
                                                                <select name="position" class="form-control <?php if (form_error('position')) { ?> is-invalid <?php } ?>"> 
                                                                    <?php
                                                                    for ($i = (count($banner) + 1); $i >= 1; $i--) {
                                                                        echo "<option " . ($updata[0]->position == $i ? 'selected' : '') . " value='" . $i . "'>" . $i . "</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('position')) {
                                                                        echo form_error('position');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label">Select Banner <span style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span> </label>
                                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                        <div class="input-append"> 
                                                                            <input type="file" id="file" onchange="readURL(this, 'blah');$('#updateStatus').val('yes');" name="banner" class="" accept="image/*">  
                                                                            <input type="hidden" id="updateStatus" name="updateStatus" />
                                                                            <input type="hidden" value="<?php echo $updata[0]->path; ?>" name="oldPath" />
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    if ($updata[0]->path):
                                                                        ?>
                                                                        <img src="<?php echo base_url($updata[0]->path); ?>" class="mt-20 center-block" width="50" id="blah" />
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <img class="mt-20 center-block" width="50" id="blah" />
                                                                    <?php
                                                                    endif;
                                                                    ?> 
                                                                    <p class="error-text file-error" style="display: none">Select a valid file!</p> 
                                                                    <div class="error-text file-error" style="display: none">Select a valid file!</div> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                    </div>
                                                    <footer class="card-footer">
                                                        <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Update Banner</button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" >Cancel</a> 
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                                <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>  
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form name="add" method="post" enctype="multipart/form-data" onsubmit="return filevalidate();">
                                                    <div class="card-body">
                                                        <div class="row">  
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Title</label>
                                                                    <input type="text" name="title" value="<?php
                                                                    if (set_value('title') && !isset($success)) {
                                                                        echo set_value('title');
                                                                    }
                                                                    ?>" placeholder="Enter Banner Title" class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('title')) {
                                                                            echo form_error('title');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label class="control-label">Position</label>
                                                                <select name="position" class="form-control <?php if (form_error('position')) { ?> is-invalid <?php } ?>"> 
                                                                    <?php
                                                                    for ($i = (count($banner) + 1); $i >= 1; $i--) {
                                                                        echo "<option value='" . $i . "'>" . $i . "</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('position')) {
                                                                        echo form_error('position');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="control-label">Sub Title</label>
                                                                    <input type="text" name="subtitle" value="<?php
                                                                    if (set_value('subtitle') && !isset($success)) {
                                                                        echo set_value('subtitle');
                                                                    }
                                                                    ?>" placeholder="Enter Banner Sub Title" class="form-control <?php if (form_error('subtitle')) { ?> is-invalid <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('subtitle')) {
                                                                            echo form_error('subtitle');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label">Select Banner <span style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span> </label>
                                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                        <div class="input-append"> 
                                                                            <input type="file" id="file" onchange="readURL(this, 'blah');" name="banner" class="" accept="image/*">  
                                                                        </div>
                                                                    </div>
                                                                    <img src="" class="mt-20 center-block" width="50" id="blah" />
                                                                    <div class="error-text file-error" style="display: none">Select a valid file!</div> 
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="card-footer">
                                                        <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="add">Add Banner</button> 
                                                        <button class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light" type="reset">Reset Form</button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                                <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?> 
                                    </div>
                                </div>
                                <!-- << ADD/EDIT Data END
                                ================================================== -->
                                <?php
                            else:
                                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                    ?> 
                                    <!-- >> Table Data Start
                                    ================================================== -->
                                    <div class="col-12 col-md-12 col-lg-12"> 
                                        <div class="card">
                                            <div class="card-body">  
                                                <div class="">   <!-- table-responsive -->
                                                    <table class="table mb-none table-hover" id="bannerTable">
                                                        <thead>
                                                            <tr> 
                                                                <th>Title</th>
                                                                <th>Sub Title</th>
                                                                <th>Position</th>
                                                                <th>Banner</th>
                                                                <?php
                                                                if ($permission['all'] || $permission['edit']):
                                                                    echo '<th>Edit</th>';
                                                                endif;
                                                                if ($permission['all'] || $permission['delete']):
                                                                    echo '<th>Delete</th>';
                                                                endif;
                                                                ?>  
                                                            </tr>
                                                        </thead> 
                                                    </table> 
                                                </div> 
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- << Table Data End
                                    ================================================== --> 
                                    <?php
                                else:
                                    $this->load->view('admin/common/access_denied');
                                endif;
                            endif;
                            ?>
                        </div>
                    </div>
                </section>
            </div>  
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer  ?> 
        </div>
    </div>  
    <?php
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?> 
    <script>
        // List column which will be display in the table
        const column = [
            {data: 'title'},
            {data: 'subtitle'},
            {data: 'position'},
            {data: 'path'},
<?php
if ($permission['all'] || $permission['edit']):
    echo '{data: "edit"},';
endif;
if ($permission['all'] || $permission['delete']):
    echo '{data: "delete"},';
endif;
?>
        ];
        // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE] 
        getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'path');
    </script>
</body> 

<?php
$web_data = $this->md->select('tbl_web_data')[0];
?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>Invoice No.: <?php echo $invoiceData ? $invoiceData[0]->transaction_id : '' ?> - All Shine Car
        Groomers</title>
    <base href="<?php echo base_url('assets/'); ?>"/>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">
    <!-- Report CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('admin_asset/'); ?>/css/report.css"/>
</head>
<body>
<!-- Define header and footer blocks before your content -->
<!--<header>-->
<!--    Dronachryas-->
<!--</header>-->

<footer>
    <div class="page-counter">
        Transaction ID: <span class="pagenum"><?php echo $invoiceData ? $invoiceData[0]->transaction_id : '' ?></span>
    </div>
    <div class="clearfix"></div>
</footer>

<!-- Wrap the content of your PDF inside a main tag -->
<main>
    <!-- Page - 1 -->
    <div style="page-break-after: never;" class="page-1">
        <div align="center">
            <h1 class="mb-10 text-transform-uppercase letter-spacing-3 font-weight-100">
                || <?php echo $web_data ? $web_data->project_name : ''; ?> ||</h1>
            <p>Phone Number: <?php echo $web_data ? $web_data->phone : ''; ?></p>
            <p><?php echo $web_data ? $web_data->address : ''; ?></p>
        </div>
        <table cellspacing="0" cellpadding="10">
            <tr style="border:1px solid #000;">
                <td>
                    <b class="subheading">Bill To</b>
                    <b>Name: </b><?php echo $invoiceData ? ucfirst($invoiceData[0]->fname) : '' ?><br/>
                    <b>Email: </b><?php echo $invoiceData ? $invoiceData[0]->email : 'not mentioned!' ?> <br/>
                    <b>Phone: </b><?php echo $invoiceData ? $invoiceData[0]->phone : 'not mentioned!' ?><br/>
                    <b>Address: </b><?php echo $invoiceData ? ucfirst($invoiceData[0]->address) : 'not mentioned!' ?>
                </td>
                <td style="border:1px solid #000;width: 50%">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="strong">Invoice#</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? $invoiceData[0]->order_id : '' ?></td>
                        </tr>
                        <tr>
                            <td class="strong">Invoice Date</td>
                            <td>:</td>
                            <td><?php echo $invoiceData ? $invoiceData[0]->entry_date : '' ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td colspan="2">
                    <table border="1" bordercolor="#f5f5f5" cellspacing="0" cellpadding="3" width="100%">
                        <tr>
                            <th width="5%">No</th>
                            <th width="60%">Item</th>
                            <th width="5%">Qty</th>
                            <th width="15%">Price</th>
                            <th width="15%">Net Price</th>
                        </tr>
                        <?php
                        $items = $this->md->select_where('tbl_transaction', array('bill_id' => $invoiceData[0]->bill_id));
                        if ($items) {
                            foreach ($items as $key => $item) {
                                ?>
                                <tr class="text-center">
                                    <td><?php echo $key + 1; ?></td>
                                    <td>
                                        <?php
                                        echo $this->md->getItemName('tbl_product', 'product_id', 'title', $item->product_id);
                                        ?>
                                    </td>
                                    <td><?php echo $item->qty; ?></td>
                                    <td><span
                                            style="font-family: DejaVu Sans; sans-serif;">&#8377;</span><?php echo $item->price; ?>
                                    </td>
                                    <td><span
                                            style="font-family: DejaVu Sans; sans-serif;">&#8377;</span><?php echo $item->netprice; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="4" align="right">Sub Total</td>
                            <td align="center">
                                <span
                                    style="font-family: DejaVu Sans; sans-serif;">&#8377;</span><?php echo $invoiceData ? ($invoiceData[0]->netprice ? number_format($invoiceData[0]->netprice,) : '') : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">Discount</td>
                            <td align="center">
                                <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span>0.00
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">Tax</td>
                            <td align="center">
                                <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span>0.00
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">Shipping</td>
                            <td align="center">
                                <span style="font-family: DejaVu Sans; sans-serif;">&#8377;</span>0.00
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" class="font-20 font-weight-bold">Grand Total</td>
                            <td align="center" class="font-20 font-weight-bold">
                                <span
                                    style="font-family: DejaVu Sans; sans-serif;">&#8377;</span><?php echo $invoiceData ? ($invoiceData[0]->netprice ? number_format($invoiceData[0]->netprice, 2) : '') : '' ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border:1px solid #000;">
                <td align="center">
                    <Br/>
                    <i>Thanks for your business!</i>
                    <Br/><Br/><Br/>
                </td>
                <td align="center" style="border:1px solid #000;width: 50%">
                    <br/>
                    <i> Authorized Signature</i>
                    <Br/><Br/><Br/>
                </td>
            </tr>
        </table>
    </div>

</main>
</body>
</html>
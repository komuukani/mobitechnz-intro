<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$fields = $this->md->select('tbl_category');    // get all category
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" id="productTitle" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            } else {
                                                                echo $updata[0]->title;
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Category</label>
                                                            <select name="category_id"
                                                                    class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                <?php
                                                                if (!empty($fields)):
                                                                    foreach ($fields as $fields_val):
                                                                        ?>
                                                                        <option <?php echo ($updata[0]->category_id == $fields_val->category_id) ? 'selected' : ''; ?>
                                                                            value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                    <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </select>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('category_id')) {
                                                                    echo form_error('category_id');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <div class="row">
                                                                <label  class="control-label col-sm-6">Product Type</label>
                                                                <div class="col-sm-10">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="zoom-14" type="radio" name="product_type" id="dineIn" value="Dine In" <?php echo ($updata[0]->product_type == "Dine In") ? 'checked' : ''; ?>>
                                                                        <label class="form-check-label ml-1" for="dineIn">Dine in</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="zoom-14" type="radio" name="product_type" id="takeAway" value="Take Away" <?php echo ($updata[0]->product_type == "Take Away") ? 'checked' : ''; ?>>
                                                                        <label class="form-check-label ml-1" for="takeAway">Take away</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="zoom-14" type="radio" name="product_type" id="both" value="Both" <?php echo ($updata[0]->product_type == "Both") ? 'checked' : ''; ?>>
                                                                        <label class="form-check-label ml-1" for="both">Both</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product ingredients</label>
                                                            <textarea style="height: 100px" name="ingredients"
                                                                      class="form-control <?php if (form_error('description')) { ?> is-invalid <?php } ?>"
                                                                      placeholder="Enter Product Description"><?php
                                                                if (set_value('ingredients') && !isset($success)) {
                                                                    echo set_value('ingredients');
                                                                } else {
                                                                    echo $updata[0]->ingredients;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ingredients')) {
                                                                    echo form_error('ingredients');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Price</label>
                                                            <input name="price" step="any" class="form-control" id="price" type="number" value="<?php
                                                            if (set_value('price') && !isset($success)) {
                                                                echo set_value('price');
                                                            } else {
                                                                echo $updata[0]->price;
                                                            }
                                                            ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('price')) {
                                                                    echo form_error('price');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Select Product Photo <span
                                                                    style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                            </label>
                                                            <div class="fileupload fileupload-new"
                                                                 data-provides="fileupload">
                                                                <div class="input-append">
                                                                    <input type="file"
                                                                           onchange="readURL(this, 'blah');$('#updateStatus').val('yes');"
                                                                           name="product" accept="image/*">
                                                                    <input type="hidden" id="updateStatus"
                                                                           name="updateStatus"/>
                                                                    <input type="hidden"
                                                                           value="<?php echo $updata[0]->path; ?>"
                                                                           name="oldPath"/>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ($updata[0]->path):
                                                                ?>
                                                                <img src="<?php echo base_url($updata[0]->path); ?>"
                                                                     class="mt-20 center-block" width="50" id="blah"/>
                                                            <?php
                                                            else:
                                                                ?>
                                                                <img class="mt-20 center-block" width="50" id="blah"/>
                                                            <?php
                                                            endif;
                                                            ?>
                                                            <p class="error-text file-error" style="display: none">
                                                                Select a valid file!</p>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Product
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" id="productTitle" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Category</label>
                                                                <select name="category_id"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('category_id')) {
                                                                        echo form_error('category_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <div class="row">
                                                                <label  class="control-label col-sm-6">Product Type</label>
                                                                <div class="col-sm-10">
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="zoom-14" type="radio" name="product_type" id="dineIn" value="Dine In" checked>
                                                                        <label class="form-check-label ml-1" for="dineIn">Dine in</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="zoom-14" type="radio" name="product_type" id="takeAway" value="Take Away">
                                                                        <label class="form-check-label ml-1" for="takeAway">Take away</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="zoom-14" type="radio" name="product_type" id="both" value="Both">
                                                                        <label class="form-check-label ml-1" for="both">Both</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product ingredients</label>
                                                            <textarea style="height: 100px" name="ingredients"
                                                                      class="form-control <?php if (form_error('description')) { ?> is-invalid <?php } ?>"
                                                                      placeholder="Enter Product Description"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('ingredients')) {
                                                                    echo form_error('ingredients');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Price</label>
                                                            <input name="price" step="any" class="form-control" id="price" type="number">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('price')) {
                                                                    echo form_error('price');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Select Product Photo <span
                                                                    style="font-size: 12px" class="text-info">*(Upload only .jpg | .jpeg | .png files.)</span>
                                                            </label>
                                                            <div class="fileupload fileupload-new"
                                                                 data-provides="fileupload">
                                                                <div class="input-append">
                                                                    <input type="file" id="file"
                                                                           onchange="readURL(this, 'blah');"
                                                                           name="product" accept="image/*">
                                                                </div>
                                                            </div>
                                                            <img class="mt-20 center-block" width="50"
                                                                 id="blah"/>
                                                            <p class="error-text file-error" style="display: none">
                                                                Select a valid file!</p>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Product
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="productTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Category</th>
                                                        <th>Title</th>
                                                        <th>Photo</th>
                                                        <th class="none">Entry Date</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['status']):
                                                            echo '<th>Action</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'status'},
        {data: 'category'},
        {data: 'title'},
        {data: 'path'},
        {data: 'datetime'},
        <?php
        if ($permission['all'] || $permission['status']):
            echo '{data: "action"},';
        endif;
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'path', true);

    // Blog Title convert into slug (Replace space with dash)
    jQuery(document).on("keyup blur", "#productTitle", function () {
        let str = $('#productTitle').val();
        jQuery('#productSlug').val('Loading...');
        var data = {
            str: str
        };
        var url = "<?php echo base_url('Admin/Pages/getSlug'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#productSlug').val(data);
        });
    });
</script>
</body>  
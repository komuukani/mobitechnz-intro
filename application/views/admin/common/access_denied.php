<div class="card-body">
    <div class="row">  
        <div class="col-md-12">
            <div class="alert alert-warning alert-has-icon p-4">
                <div class="alert-icon"><i class="fas fa-exclamation-triangle"></i></div>
                <div class="alert-body">
                    <div class="alert-title">Attention!</div> 
                    <p><strong>Access Denied</strong>, You don’t have permission to access this page! For more information, contact to super admin. <br/> Or you can drop mail of this warning to super admin.</p> 
                </div>
            </div> 
        </div>
    </div>
</div>
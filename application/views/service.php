<?php
echo $page_head;
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

    <!-- Document Wrapper
        ============================================= -->
    <main>
        <?php echo $page_breadcumb; ?>
        <!-- Service Section - Start
          ================================================== -->
        <div class="service_section section_space pb-0">
            <div class="container">

                <div class="service_items_wrap">
                    <div class="row"> <?php
                        if (!empty($service)) {
                        foreach ($service as $service_data) {
                        $url = base_url('service/' . strtolower($service_data->slug));
                         $service_desc = substr($service_data->description, 0, 400);
                        ?>
                        <div class="col col-lg-4 col-md-6">
                            <div class="service_card_layout style_2 bordered p-3">
                                <div class="item_image">
                                    <a class="image_wrap" href="<?php echo $url; ?>">
                                        <img src="<?php echo base_url($service_data->path); ?>" alt="<?php echo $service_data->title; ?>"
                                        style="width:100%;height: 210px;object-fit: cover">
                                    </a>
                                </div>
                                <div class="item_content">
                                    <h4 class="item_title"><?php echo $service_data->title; ?></h4>
<!--                                    <p>--><?php //echo $service_desc ? $service_desc : ''; ?><!--</p>-->
                                    <a class="btn danger_border btn_rounded mt-0" href="<?php echo $url; ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                            <?php
                        }
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- Service Section - End
          ================================================== -->
        <!-- Brond Section - Start
     ================================================== -->
        <div class="brand_section section_space">
            <div class="container">

                <ul class="product_brands_list border_style ul_li_center">
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/mi.png" alt="Xiaomi Mi Brand Logo">
                  </small>
                  <small class="item_title">Xiaomi</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/samsung.png" alt="Samsung Brand Logo">
                  </small>
                  <small class="item_title">Samsung</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/vivo.png" alt="Vivo Brand Logo">
                  </small>
                  <small class="item_title">Vivo</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/motorola.png" alt="Motorola Brand Logo">
                  </small>
                  <small class="item_title">Motorola</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/oppo.png" alt="Oppo Brand Logo">
                  </small>
                  <small class="item_title">Oppo</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/micromax.png" alt="Micromax Brand Logo">
                  </small>
                  <small class="item_title">Micromax</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/apple.png" alt="Apple Brand Logo">
                  </small>
                  <small class="item_title">Apple</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/lava.png" alt="Lava Brand Logo">
                  </small>
                  <small class="item_title">Lava</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/lg_butterfly.png" alt="LG Butterfly Brand Logo">
                  </small>
                  <small class="item_title">LG Butterfly</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_title">View More Brands</small>
                </span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
        <!-- Brond Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>


<!-- end of #content -->


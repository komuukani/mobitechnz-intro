<?php
$admin_data = $this->md->select('tbl_admin')[0];
$web_data = ($web_data) ? $web_data[0] : '';
$meta_data = $this->md->select_where('tbl_seo', array('page' => $page));

$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
$services = $this->md->select('tbl_service');
?>
<!doctype html>
<html lang="en">
<!--<![endif]-->
<?php ob_start(); ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?php echo base_url("/"); ?>"/>
    <?php
    if (!empty($meta_data)) {
        ?>
        <meta name="author" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="title" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="description" content="<?php echo $meta_data ? $meta_data[0]->description : ''; ?>">
        <meta name="keywords" content="<?php echo $meta_data ? $meta_data[0]->keyword : ''; ?>">
        <title><?php echo $meta_data ? ($meta_data[0]->title . " | " . $web_data->project_name) : (' | ' . $web_data->project_name); ?></title>
        <?php
    }
    if (!empty($service_data)) {
        ?>
        <meta name="author" content="<?php echo $service_data ? $service_data[0]->meta_title : ''; ?>">
        <meta name="title"
              content="<?php echo $service_data ? ($service_data[0]->meta_title ? $service_data[0]->meta_title : $service_data[0]->title) : ''; ?>">
        <meta name="description" content="<?php echo $service_data ? $service_data[0]->meta_desc : ''; ?>">
        <meta name="keywords" content="<?php echo $service_data ? $service_data[0]->meta_keyword : ''; ?>">
        <title><?php echo $service_data ? ($service_data[0]->meta_title . " | " . $web_data->project_name) : (' | ' . $web_data->project_name); ?></title>
        <?php
    } else {
        ?>
        <title><?php echo $page_title . ' | ' . $web_data->project_name; ?></title>
        <?php
        if (isset($blog_data)) {
            ?>
            <meta name="title"
                  content="<?php echo $blog_data[0]->meta_title ? $blog_data[0]->meta_title : $blog_data[0]->title; ?>">
            <meta name="description" content="<?php echo $blog_data[0]->meta_desc; ?>">
            <meta name="keywords" content="<?php echo $blog_data[0]->meta_keyword; ?>">
            <?php
        }
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo $page_title . " - " . $web_data->project_name; ?></title>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">

    <!-- Fraimwork - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <!-- Icon Font - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">

    <!-- Animation - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

    <!-- Carousel - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">

    <!-- Video & Image Popup - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">

    <!-- Select Option - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">

    <!-- Pricing Range - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css">

    <!-- Custom - CSS Include -->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<!-- page wrapper -->
<?php $page_head = ob_get_clean(); ?>

<?php ob_start(); ?>
<!-- Back To Top - Start -->
<div class="backtotop">
    <a href="#" class="scroll">
        <i class="far fa-arrow-up"></i>
    </a>
</div>
<!-- Back To Top - End -->
<!--  Preloader  -->
<div id="preloader"></div>

<header class="header_section header_boxed">
    <div class="header_top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col col-lg-4">
                    <ul class="header_icon_list_2 ul_li header_icon_list_2_left">
                        <li>
                            <a href="mailto:<?php echo($web_data->email_address); ?>">
                                <i class="fal fa-envelope"></i>
                                <span>
                                  <small>Email Us:</small>
                                  <span class="__cf_email__"><?php echo $web_data ? $web_data->email_address : ''; ?></span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <span>
                                <ul class="social_icon social_round ul_li">
                                    <li><a href="<?php echo $web_data ? $web_data->facebook : ''; ?>"><i
                                                    class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="<?php echo $web_data ? $web_data->twitter : ''; ?>"><i
                                                    class="fab fa-twitter"></i></a></li>
                                    <li><a href="<?php echo $web_data ? $web_data->instagram : ''; ?>"><i
                                                    class="fab fa-instagram"></i></a></li>
                                    <li><a href="<?php echo $web_data ? $web_data->linkedin : ''; ?>"><i
                                                    class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                             </span>
                        </li>
                    </ul>
                </div>

                <div class="col col-lg-4">
                    <div class="header_quick_link text-center">
                        Sale your old handset through. <a href="javascript:void(0)"
                                                          class="text-white text-decoration-underline">Click here</a>
                    </div>
                </div>

                <div class="col col-lg-4">
                    <ul class="header_icon_list_2 ul_li float-lg-end header_icon_list_2_right">
                        <li>
                            <i class="fal fa-map-marker-alt"></i>
                            <span>
                    <small>Contact Us:</small>
                    <?php echo $web_data ? $web_data->address : ''; ?>
                  </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="header_boxed_wrap">
            <div class="site_logo">
                <a href="<?php echo base_url(); ?>">
                    <img src="<?php echo base_url($web_data ? $web_data->logo : FILENOTFOUND); ?>" class="site-logo-img"
                         alt="<?php echo $web_data ? $web_data->project_name : ''; ?>">
                </a>
            </div>

            <nav class="main_menu navbar navbar-expand-lg">
                <button class="mobile_menu_btn navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#main_menu_dropdown" aria-controls="main_menu_dropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <i class="fal fa-bars"></i>
                </button>
                <div class="main_menu_inner collapse navbar-collapse" id="main_menu_dropdown">
                    <ul class="main_menu_list ul_li text-uppercase">
                        <li><a href="<?php echo base_url(); ?>">Home</a>
                        </li>
                        <li><a href="<?php echo base_url('aboutus'); ?>">About</a></li>
                        <li class="dropdown">
                            <a class="nav-link" href="<?php echo base_url('service'); ?>" id="service_submenu"
                               role="button"
                               aria-expanded="false">
                                Services
                            </a>
                            <ul class="submenu dropdown-menu" aria-labelledby="service_submenu">
                                <?php
                                if (!empty($services)):
                                    foreach ($services as $services_data):
                                        $url = base_url('service/' . strtolower($services_data->slug));
                                        ?>
                                        <li><a href="<?php echo $url; ?>"><?php echo $services_data->title; ?></a></li>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('gallery'); ?>">Gallery</a></li>
                        <li><a href="<?php echo base_url('contact'); ?>">Contact</a></li>
                    </ul>
                </div>

                <ul class="header_btns_group ul_li">
                    <li>
                        <button type="button" class="search_btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.667" height="19.666"
                                 viewBox="0 0 19.667 19.666">
                                <g transform="translate(0 -0.035)">
                                    <path
                                            d="M8.216,16.459a8.2,8.2,0,0,0,5.037-1.73l4.684,4.686a1.021,1.021,0,0,0,1.444-1.444L14.7,13.285a8.212,8.212,0,1,0-6.481,3.174ZM3.853,3.885a6.171,6.171,0,1,1,0,8.727h0a6.149,6.149,0,0,1-.032-8.7l.032-.032Z"
                                            transform="translate(0 0)" fill="#000323"/>
                                </g>
                            </svg>
                        </button>
                    </li>
                    <li>
                        <div class="header_hotline">
                            Call us for any question
                            <strong><a href="tel:+<?php echo $web_data ? $web_data->phone : ''; ?>"
                                       data-text-color="#15C0FA"><?php echo $web_data ? $web_data->phone : ''; ?></a></strong>
                        </div>
                    </li>
                    <li><a class="btn btn_danger" href="<?php echo base_url() ?>">Get Enquiry</a></li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="search_body">
        <button type="button" class="search_close">
            <i class="fal fa-times"></i>
        </button>
        <div class="container">
            <form action="#">
                <h3>Search anything you want.</h3>
                <div class="form_item m-0">
                    <input type="search" name="search" placeholder="Search Here...">
                    <button type="button" class="search_submit_btn">Search Now</button>
                </div>
            </form>
        </div>
    </div>
</header>

<!-- Main Header Area End Here -->
<?php $page_header = ob_get_clean(); ?>
<?php ob_start(); ?>
<section class="breadcrumb_section text-center"
         style="background-image: url(assets/images/backgrounds/bg_6.jpg);">
    <div class="container">
        <h1 class="page_title text-uppercase"><?php echo ucfirst($page_title); ?></h1>
        <ul class="breadcrumb_nav ul_li_center text-uppercase">
            <li><a href="<?php echo base_url(); ?>">HOME</a></li>
            <?php
            if ($page == 'service_details') {
                ?>
                <li><a href="<?php echo base_url('service'); ?>">Service</a></li>
                <?php
            }
            ?>
            <li><?php echo ucfirst($page_title); ?></li>
        </ul>
    </div>
</section>
<!-- Banner End -->
<?php $page_breadcumbs = ob_get_clean(); ?>
<?php ob_start(); ?>

<!-- Footer Area Start -->
<footer class="footer_section footer_3">
    <div class="footer_widget_area">
        <div class="col">
            <div class="footer_widget contact_info_block">
                <h3 class="footer_widget_title text-white">Get in Touch</h3>
                <ul class="ul_li_block">
                    <li class="item-support">
                        <div class="item_icon">
                            <i class="flaticon-headphones"></i>
                        </div>
                        <div class="item_content">
                  <span>
                    Call to Support
                  </span>
                            <span>
                    <a class="hot_line text-white"
                       href="tel:<?php echo $web_data ? $web_data->phone : ''; ?>"><?php echo $web_data ? $web_data->phone : ''; ?></a>
                  </span>
                        </div>
                    </li>
                    <li>
                        <div class="item_icon">
                            <i class="flaticon-email"></i>
                        </div>
                        <div class="item_content">
                  <span>
                    <a href="mailto:<?php echo $web_data ? $web_data->email_address : ''; ?>"><span
                                class="__cf_email__"><?php echo $web_data ? $web_data->email_address : ''; ?></span></a>
                  </span>
                        </div>
                    </li>
                    <li>
                        <div class="item_icon">
                            <i class="flaticon-location"></i>
                        </div>
                        <div class="item_content">
                  <span>
                   <?php echo $web_data ? $web_data->address : ''; ?>
                  </span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col">
            <div class="footer_widget useful_links list_left_border">
                <h3 class="footer_widget_title text-white">About Company</h3>
                <ul class="ul_li_block">
                    <li><a href="<?php echo base_url('aboutus'); ?>">About Us</a></li>
                    <li><a href="<?php echo base_url('service'); ?>">Services</a></li>
                    <li><a href="<?php echo base_url('gallery'); ?>">Gallery</a></li>
                    <li><a href="<?php echo base_url('contact'); ?>">Contact</a></li>
                    <li><a href="<?php echo base_url('policy'); ?>">Privacy policy</a></li>
                    <li><a href="<?php echo base_url('terms'); ?>">Terms & Condition</a></li>
                </ul>
            </div>
        </div>

        <div class="col">
            <div class="footer_widget useful_links list_left_border">
                <h3 class="footer_widget_title text-white">Our Services</h3>
                <ul class="ul_li_block">
                    <?php
                    if (!empty($services)):
                        foreach ($services as $services_data):
                            $url = base_url('service/' . strtolower($services_data->slug));
                            ?>
                            <li><a href="<?php echo $url; ?>"><?php echo $services_data->title; ?></a></li>
                        <?php
                        endforeach;
                    endif;
                    ?>
                </ul>
            </div>
        </div>

        <div class="col">
            <div class="footer_widget newsletter_area_2">
                <h3 class="footer_widget_title text-white">Newsletter</h3>
                <form action="#">
                    <div class="form_item form_rounded">
                        <label for="newsletter_email"><i class="flaticon-email"></i></label>
                        <input id="newsletter_email" type="email" name="email" placeholder="Your Email">
                    </div>
                    <button type="submit" class="btn btn_danger btn_rounded">Subscribe <i
                                class="flaticon-paper-plane"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="site_logo text-center mb-0">
        <a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url($web_data ? $web_data->logo : FILENOTFOUND); ?>"
                 alt="<?php echo $web_data ? $web_data->project_name : ''; ?>">
        </a>
    </div>
    <div class="footer_bottom align-items-center bg-white">
        <div class="col">
            <p class="copyright_text"><?php echo $web_data ? $web_data->footer : ''; ?></p>
        </div>
        <div class="col">
            <ul class="social_icon ul_li_center">
                <li><a href="<?php echo $web_data ? $web_data->facebook : ''; ?>"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li><a href="<?php echo $web_data ? $web_data->twitter : ''; ?>"><i class="fab fa-twitter"></i></a></li>
                <li><a href="<?php echo $web_data ? $web_data->instagram : ''; ?>"><i class="fab fa-instagram"></i></a>
                </li>
                <li><a href="<?php echo $web_data ? $web_data->linkedin : ''; ?>"><i class="fab fa-linkedin-in"></i></a>
                </li>
            </ul>
        </div><div class="col"></div>

    </div>
</footer>

<?php $page_footer = ob_get_clean(); ?>
<?php ob_start(); ?>
<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-dropdown-ml-hack.js"></script>

<!-- Carousel - Jquery Include -->
<script src="assets/js/slick.min.js"></script>

<!-- Video & Image Popup - Jquery Include -->
<script src="assets/js/magnific-popup.min.js"></script>
<script src="assets/js/beforeafter.jquery-1.0.0.min.js"></script>


<!-- Select Option - Jquery Include -->
<script src="assets/js/nice-select.min.js"></script>

<!-- BG Parallax - Jquery Include -->
<script src="assets/js/parallaxie.js"></script>

<!-- Counter Up - Jquery Include -->
<script src="assets/js/waypoint.js"></script>
<script src="assets/js/counterup.min.js"></script>

<!-- Animation - Jquery Include -->
<script src="assets/js/wow.min.js"></script>

<!-- Pricing Range - Jquery Include -->
<script src="assets/js/jquery-ui.js"></script>

<!-- Custom - Jquery Include -->
<script src="assets/js/main.js"></script>
<?php $page_footerscript = ob_get_clean(); ?>
<?php
if (isset($success)) {
    ?>
    <script>
        $.notify('<?php echo $success; ?>', 'success');
    </script>
    <?php
}
if (isset($error)) {
    ?>
    <script>
        $.notify('<?php echo $error; ?>', 'error');
    </script>
    <?php
}

$this->load->view($page, array(
    'page_title' => $page_title,
    'page_head' => $page_head,
    'page_header' => $page_header,
    'page_breadcumb' => ($page_breadcumb) ? $page_breadcumbs : '',
    'page_footer' => $page_footer,
    'page_footerscript' => $page_footerscript
));
?>
<style>
    .error-text {
        width: 100%;
        margin-top: 5px;
        font-size: 13px;
        color: #dc3545;
        background: #ff4073e6;
        padding-left: 10px;
    }

    .error-text p {
        margin-bottom: 0 !important;
        color: #ffe7e9 !important;
        margin-left: 12px;
        font-weight: 500;
    }

</style>
<script>


</script>
</html>
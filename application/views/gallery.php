<?php
echo $page_head;
$gallery = $this->md->select('tbl_gallery');
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

    <!-- Document Wrapper
        ============================================= -->
    <main>
        <?php echo $page_breadcumb; ?>
        <!-- About Section - Start
          ================================================== -->
        <div class="about_section section_space">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer_widget footer_instagram">
                            <div class="row zoom-gallery ul_li">
                                <?php
                                if (!empty($gallery)):
                                    foreach ($gallery as $gallery_data):
                                        ?>
                                        <li class="col-md-4">
                                            <div class="mb-30">
                                                <a class="popup_image"
                                                   href="<?php echo base_url($gallery_data ? $gallery_data->path : FILENOTFOUND); ?>">
                                                    <img src="<?php echo base_url($gallery_data ? $gallery_data->path : FILENOTFOUND); ?>"
                                                         alt="<?php echo $gallery_data->title; ?>"
                                                         title="<?php echo $gallery_data->title; ?>"
                                                         style="width:100%;height: 250px;object-fit: cover">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                                <h5 class="bg-F5F5F5 p-2"><?php echo ucfirst($gallery_data->title); ?></h5>
                                            </div>
                                        </li>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- End intro center -->
                </div>
            </div>
        </div>
        <!-- About Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
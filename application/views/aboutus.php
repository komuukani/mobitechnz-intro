<?php
echo $page_head;
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

<!-- Document Wrapper
    ============================================= -->
    <main>
    <?php echo $page_breadcumb; ?>
        <!-- About Section - Start
           ================================================== -->
        <section class="about_section section_space">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6">
                        <div class="about_image_4">
                            <img src="assets/images/about/about_img_5.jpg" alt="About Company">
                        </div>
                    </div>
                    <div class="col col-lg-6">
                        <div class="about_content">
                            <div class="section_title">
                                <h2 class="sub_title">About Company</h2>
                                <h3 class="title_text">
                                    Repair Services For Your MObile & Computer .
                                </h3>
                                <ul class="list-unstyled mb-20">
                                    <li class="text-theme-colored1"><i class="fa fa-check-circle text-theme-colored1 mr-10"></i> Best mobile phone
                                        repair services at affordable prices
                                    </li>
                                    <li class="text-theme-colored1"><i class="fa fa-check-circle text-theme-colored1 mr-10"></i> Computor repair
                                        services fast &amp; quick fix
                                    </li>
                                </ul>
                                <p class="mb-0">
                                    Mobi Tech is a professional platform for mobile phone repairs. This platform integrates all relevant resources online and offline, aims to provide NZ consumers with convenient, secure, reliable, and prompt repair services for their digital devices.
                                </p>
                            </div>
                            <div class="row">
                                <div class="col col-lg-6">
                                    <div class="policy_item_2">
                                        <div class="item_icon">
                                            <i class="flaticon-smartphone-1"></i>
                                        </div>
                                        <div class="item_content">
                                            <h3 class="item_title">Mobile & Computer</h3>
                                            <p class="mb-0">
                                                Mobile & Computer repair center for the long established.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="policy_item_2">
                                        <div class="item_icon">
                                            <i class="flaticon-car-repair-1"></i>
                                        </div>
                                        <div class="item_content">
                                            <h3 class="item_title">Car & Vehicle</h3>
                                            <p class="mb-0">
                                                Car & vehicle repair center for the long established.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="about_images_group ul_li">
                                <li>
                                    <img src="assets/images/about/about_img_6.jpg" alt="Working Image">
                                </li>
                                <li>
                                    <img src="assets/images/about/about_img_7.jpg" alt="Working Image">
                                </li>
                                <li>
                                    <img src="assets/images/about/about_img_8.jpg" alt="Working Image">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section - End
          ================================================== -->

        <!-- Policy Section - Start
          ================================================== -->
        <div class="policy_section" data-bg-color="#000323">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-3 col-md-4 col-sm-6">
                        <div class="policy_item_3 style_2">
                            <div class="item_icon">
                                <i class="flaticon-support"></i>
                            </div>
                            <h3 class="item_title text-white">Repairing Service</h3>
                            <p data-text-color="#858DA1">
                                Computer repair when an unknown printer took a galley of type and scrambled computer
                            </p>
                        </div>
                    </div>

                    <div class="col col-lg-3 col-md-4 col-sm-6">
                        <div class="policy_item_3 style_2">
                            <div class="item_icon">
                                <i class="flaticon-sales"></i>
                            </div>
                            <h3 class="item_title text-white">Accessories Sales</h3>
                            <p data-text-color="#858DA1">
                                Computer repair when an unknown printer took a galley of type and scrambled computer
                            </p>
                        </div>
                    </div>

                    <div class="col col-lg-3 col-md-4 col-sm-6">
                        <div class="policy_item_3 style_2">
                            <div class="item_icon">
                                <i class="flaticon-customer-support"></i>
                            </div>
                            <h3 class="item_title text-white">Customer Support</h3>
                            <p data-text-color="#858DA1">
                                Computer repair when an unknown printer took a galley of type and scrambled computer
                            </p>
                        </div>
                    </div>

                    <div class="col col-lg-3 col-md-4 col-sm-6">
                        <div class="policy_item_3 style_2">
                            <div class="item_icon">
                                <i class="flaticon-protection-shield-with-a-check-mark"></i>
                            </div>
                            <h3 class="item_title text-white">Money Back Guarantee</h3>
                            <p data-text-color="#858DA1">
                                Computer repair when an unknown printer took a galley of type and scrambled computer
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Policy Section - End
          ================================================== -->

        <!-- About Section - Start
          ================================================== -->
        <div class="about_section section_space">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 intro_message mt40">
                        <?php
                        if (empty($aboutus)) :
                            echo "Sorry, content not available";
                        else :
                            foreach ($aboutus as $key => $aboutus_data) {
                                echo $aboutus_data->about;
                            }
                        endif;
                        ?>
                    </div>
                    <!-- End intro center -->
                </div>
            </div>
        </div>
        <!-- About Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>


<!-- end of #content -->

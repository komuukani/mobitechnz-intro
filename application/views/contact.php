<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$web_data = ($web_data) ? $web_data[0] : '';
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

    <!-- Document Wrapper
        ============================================= -->
    <main>
        <?php echo $page_breadcumb; ?>
        <!-- Contact Section - Start
       ================================================== -->
        <section class="contact_section">
            <div class="container">
                <div class="contact_wrapper">
                    <div class="row">
                        <div class="col col-lg-6">
                            <div class="content_wrap">
                                <h2>
                                    Contact Directly With Our Agents
                                </h2>
                                <p>
                                    But I must explain to you how all this mista ken idea of denouncing
                                </p>
                                <ul class="contact_info_iconbox ul_li_block">
                                    <li>
                                          <span>
                                            <i class="far fa-phone"></i>
                                          </span>
                                        <strong>
                                            Phone
                                            <small><a href="tel:<?php echo($admin_data->phone); ?>"><?php echo($admin_data->phone); ?></a></small>
                                        </strong>
                                    </li>
                                    <li>
                                          <span>
                                            <i class="far fa-envelope"></i>
                                          </span>
                                        <strong>
                                            Email
                                            <small><a href="mailto:<?php echo($admin_data->email_address); ?>"><?php echo($admin_data->email_address); ?></a></small>
                                        </strong>
                                    </li>
                                    <li>
                                          <span>
                                            <i class="far fa-map-marker-alt"></i>
                                          </span>
                                        <strong>
                                            Location
                                            <small><?php echo($admin_data->address); ?></small>
                                        </strong>
                                    </li>
                                </ul>
                                <div class="opening_time">
                                    <h3>Opening Time</h3>
                                    <ul class="ul_li_block">
                                        <li>Mon - Fri : 9.00 - 18.00</li>
                                        <li>Sat : 10.00 - 17.00</li>
                                        <li>Sun : 10.00 - 16.00</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col col-lg-6">
                            <form method="POST" autocomplete="off" novalidate>
                                <div class="form_wrap">
                                    <?php
                                    if (isset($error)) {
                                        ?>
                                        <div class="alert alert-danger p-1">
                                            <?php echo $error; ?>
                                        </div>
                                        <?php
                                    }
                                    if (isset($success)) {
                                        ?>
                                        <div class="alert alert-success p-1">
                                            <?php echo $success; ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col col-md-12">
                                            <div class="form_item">
                                                <h4 class="form_item_title">Name*</h4>
                                                <input type="text" name="fname" id="name" placeholder="Name *" required value="<?php
                                                if (set_value('fname') && !isset($success)) {
                                                    echo set_value('fname');
                                                }
                                                ?>">
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('fname')) {
                                                        echo form_error('fname');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-md-12">
                                            <div class="form_item">
                                                <h4 class="form_item_title">Phone*</h4>
                                                <input type="tel" name="phone" id="phone" placeholder="Enter Your Number..." required
                                                       value="<?php
                                                       if (set_value('phone') && !isset($success)) {
                                                           echo set_value('phone');
                                                       }
                                                       ?>">
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('phone')) {
                                                        echo form_error('phone');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-md-12">
                                            <div class="form_item">
                                                <h4 class="form_item_title">Email*</h4>
                                                <input type="email" name="email" id="email" class="form-control text" placeholder="E-mail *" value="<?php
                                                if (set_value('email') && !isset($success)) {
                                                    echo set_value('email');
                                                }
                                                ?>" required>
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('email')) {
                                                        echo form_error('email');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_item">
                                        <h4 class="form_item_title">Message*</h4>
                                        <textarea id="message" name="message" placeholder="Message *" required><?php
                                            if (set_value('message') && !isset($success)) {
                                                echo set_value('message');
                                            }
                                            ?></textarea>
                                        <div class="error-text">
                                            <?php
                                            if (form_error('message')) {
                                                echo form_error('message');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form_item">
                                        <?php
                                        if ($admin_data->captcha_visibility) :
                                            echo '<div class="g-recaptcha" data-sitekey="' . $admin_data->captcha_site_key . '"></div>';
                                        endif;
                                        ?>
                                        <span id="msg"></span>
                                    </div>
                                    <button type="submit" name="send" value="Send" class="btn btn_danger">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map">
                <div><?php echo($admin_data->map); ?></div>
            </div>
        </section>
        <!-- Contact Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
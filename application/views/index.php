<?php
echo $page_head;
$blogs = $this->md->select_limit_order('tbl_blog', 3, 'blog_id', 'desc');
$web_data = ($web_data) ? $web_data[0] : '';
$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
$reviews = $this->md->select('tbl_review');
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>
    <main>

        <!-- Banner Section - Start
          ================================================== -->
        <section class="banner_section banner_3" style="background-image: url(assets/images/shapes/shape_5.png);">
            <div class="container">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col order-last col-lg-6">
                        <div class="banner_image">
                            <img src="assets/images/banner/banner_img_1.png" alt="creative Solutions Image">
                        </div>
                    </div>

                    <div class="col col-lg-6">
                        <div class="banner_content">
                            <h1>
                                <small>One Stop Repair House</small>
                                Repair Your Phone At <span data-text-color="#15C0FA">Doorstep</span>.
                            </h1>
                            <p>
                                Reapiron is a revolutionary service designed to simplify mobile repairs without making
                                them too hard
                                on your wallet. Find the mobile.
                            </p>
                            <ul class="social_icon social_round ul_li">
                                <li><a href="<?php echo $web_data ? $web_data->facebook : ''; ?>"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php echo $web_data ? $web_data->twitter : ''; ?>"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="<?php echo $web_data ? $web_data->instagram : ''; ?>"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="<?php echo $web_data ? $web_data->linkedin : ''; ?>"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Banner Section - End
          ================================================== -->

        <!-- Brond Section - Start
          ================================================== -->
        <section class="brand_section section_space">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col col-lg-6">
                        <div class="section_title text-center">
                            <h2 class="sub_title">
                                Why Choose Us
                                <span class="under_text">Brands</span>
                            </h2>
                            <h3 class="title_text mb-0">
                                Bring More Happiness in Computer Using
                            </h3>
                        </div>
                    </div>
                </div>

                <ul class="product_brands_list ul_li_center">
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/mi.png" alt="Xiaomi Mi Brand Logo">
                  </small>
                  <small class="item_title">Xiaomi</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/samsung.png" alt="Samsung Brand Logo">
                  </small>
                  <small class="item_title">Samsung</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/vivo.png" alt="Vivo Brand Logo">
                  </small>
                  <small class="item_title">Vivo</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/motorola.png" alt="Motorola Brand Logo">
                  </small>
                  <small class="item_title">Motorola</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/oppo.png" alt="Oppo Brand Logo">
                  </small>
                  <small class="item_title">Oppo</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/micromax.png" alt="Micromax Brand Logo">
                  </small>
                  <small class="item_title">Micromax</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/apple.png" alt="Apple Brand Logo">
                  </small>
                  <small class="item_title">Apple</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/lava.png" alt="Lava Brand Logo">
                  </small>
                  <small class="item_title">Lava</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_image">
                    <img src="assets/images/brands/lg_butterfly.png" alt="LG Butterfly Brand Logo">
                  </small>
                  <small class="item_title">LG Butterfly</small>
                </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                <span>
                  <small class="item_title">View More Brands</small>
                </span>
                        </a>
                    </li>
                </ul>

            </div>
        </section>
        <!-- Brond Section - End
          ================================================== -->

        <!-- Advance Section - Start
          ================================================== -->
        <div class="advance_search_section advance_search_2 section_space decoration_wrap parallaxie"
             style="background-image: url(assets/images/backgrounds/bg_4.jpg);">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col col-lg-8">
                        <div class="section_title text-center">
                            <h3 class="title_text text-white">
                                Facing Computer Problem? Please Send a Solution Request
                            </h3>
                        </div>
                    </div>
                </div>

                <form action="#">
                    <div class="row">
                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <div class="form_item">
                                <input type="text" name="name" placeholder="Your Name">
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <div class="form_item">
                                <input type="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <div class="form_item">
                                <input type="tel" name="phone" placeholder="Phone">
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <div class="form_item">
                                <input type="text" name="location" placeholder="Location">
                            </div>
                        </div>

                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <div class="select_option clearfix">
                                <select>
                                    <option data-display="Select Brand">Select Your Option</option>
                                    <option value="1">BMW</option>
                                    <option value="2">Toyota</option>
                                    <option value="3" disabled>Mitsubishi</option>
                                    <option value="4">Honda</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-4 col-sm-6">
                            <div class="select_option clearfix">
                                <select>
                                    <option data-display="Select Problem">Select Your Option</option>
                                    <option value="1">BMW 228i 4-Door Coupe</option>
                                    <option value="2">Toyota C-HR Consumer</option>
                                    <option value="3" disabled>Mitsubishi Triton / L200</option>
                                    <option value="4">Honda Accord</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-lg-6">
                            <div class="form_item">
                                <input type="text" name="comment" placeholder="Write Note (Optional)">
                            </div>
                        </div>
                    </div>

                    <div class="single_btn_wrap text-center p-0">
                        <button type="submit" class="btn btn_danger btn_rounded">Send a Request</button>
                    </div>
                </form>
            </div>

            <div class="overlay" data-bg-color="#000323"></div>

            <div class="deco_item circle_1"></div>
            <div class="deco_item circle_2"></div>
        </div>
        <!-- Advance Section - End
          ================================================== -->

        <!-- About Section - Start
          ================================================== -->
        <div class="about_section section_space pb-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6">
                        <div class="section_title mb-2">
                            <h2 class="sub_title">
                                Why Choose Us
                                <span class="under_text">Choose Us</span>
                            </h2>
                            <h3 class="title_text mb-0">
                                20K+ Mobile Repairing Problems Solved.
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-lg-between">
                    <div class="col col-lg-6">
                        <div class="about_content">
                            <div class="row">
                                <div class="col col-lg-6">
                                    <div class="policy_item_2">
                                        <div class="item_icon">
                                            <i class="flaticon-skills"></i>
                                        </div>
                                        <div class="item_content">
                                            <h3 class="item_title">Skilled Technicians</h3>
                                            <p class="mb-0" data-text-color="#828A9B">
                                                Vehicle Repair on the Spot in the Store or at Home/Office
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="policy_item_2">
                                        <div class="item_icon">
                                            <i class="flaticon-chemical-reaction"></i>
                                        </div>
                                        <div class="item_content">
                                            <h3 class="item_title">10+ Years Experiences</h3>
                                            <p class="mb-0" data-text-color="#828A9B">
                                                Vehicle Repair on the Spot in the Store or at Home/Office
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="policy_item_2">
                                        <div class="item_icon">
                                            <i class="flaticon-padlock"></i>
                                        </div>
                                        <div class="item_content">
                                            <h3 class="item_title">Quality Guarantee</h3>
                                            <p class="mb-0" data-text-color="#828A9B">
                                                Vehicle Repair on the Spot in the Store or at Home/Office
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="policy_item_2">
                                        <div class="item_icon">
                                            <i class="flaticon-holding-hands"></i>
                                        </div>
                                        <div class="item_content">
                                            <h3 class="item_title">Trusted &amp; Recommended</h3>
                                            <p class="mb-0" data-text-color="#828A9B">
                                                Vehicle Repair on the Spot in the Store or at Home/Office
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col col-lg-5">
                        <div class="progress_bar_area">
                            <div class="progress_item">
                                <h4 class="item_title mb-0">Mobile Flash</h4>
                                <div class="progress">
                                    <div class="progress_bar wow Rx_width_80" role="progressbar" data-wow-duration="1s"
                                         data-wow-delay=".4s" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                        <span class="value_text">80%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress_item">
                                <h4 class="item_title mb-0">Country Lock</h4>
                                <div class="progress">
                                    <div class="progress_bar wow Rx_width_70" role="progressbar" data-wow-duration="1s"
                                         data-wow-delay=".4s" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                        <span class="value_text">70%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress_item">
                                <h4 class="item_title mb-0">Screen & Touch</h4>
                                <div class="progress">
                                    <div class="progress_bar wow Rx_width_90" role="progressbar" data-wow-duration="1s"
                                         data-wow-delay=".4s" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                        <span class="value_text">90%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="progress_item">
                                <h4 class="item_title mb-0">Whole Body Repair</h4>
                                <div class="progress">
                                    <div class="progress_bar wow Rx_width_95" role="progressbar" data-wow-duration="1s"
                                         data-wow-delay=".4s" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                        <span class="value_text">95%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Section - End
          ================================================== -->

        <!-- Policy Section - Start
          ================================================== -->
        <div class="policy_section section_space">
            <div class="container">
                <div class="policy_wrap">
                    <div class="col">
                        <div class="policy_item_4">
                            <div class="item_icon">
                                <i class="flaticon-support"></i>
                            </div>
                            <h3 class="item_title">Repairing Service</h3>
                        </div>
                    </div>

                    <div class="col">
                        <div class="policy_item_4">
                            <div class="item_icon">
                                <i class="flaticon-sales"></i>
                            </div>
                            <h3 class="item_title">Accessories Sales</h3>
                        </div>
                    </div>

                    <div class="col">
                        <div class="policy_item_4">
                            <div class="item_icon">
                                <i class="flaticon-customer-support"></i>
                            </div>
                            <h3 class="item_title">Customer Support</h3>
                        </div>
                    </div>

                    <div class="col">
                        <div class="policy_item_4">
                            <div class="item_icon">
                                <i class="flaticon-protection-shield-with-a-check-mark"></i>
                            </div>
                            <h3 class="item_title">Money Back Guarantee</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Policy Section - End
          ================================================== -->

        <!-- About Section - Start
          ================================================== -->
        <section class="about_section section_space bg_black">
            <div class="about_image_3">
                <img src="assets/images/about/about_img_4.jpg" alt="Person Image">
            </div>
            <div class="container">
                <div class="row justify-content-lg-end">
                    <div class="col col-lg-7">
                        <div class="about_content_2">
                            <div class="year_text">
                                <strong>10<sup>+</sup></strong>
                                <span>Years Experiences</span>
                            </div>
                            <div class="section_title mb-0">
                                <h2 class="sub_title">
                                    About Company
                                    <span class="under_text">About</span>
                                </h2>
                                <h3 class="title_text">
                                    Trusted Repairing Shop Of Complex Solutions.
                                </h3>
                                <p>
                                    All kinds of laptop, desktop computer servicing center forIt is a long established
                                    fact that a
                                    reader will be distracted by the readable computer disk. Lorem ipsum dolor sit amet,
                                    consectetur
                                    adipiscing elit. Ut elit tellus, luctus nec
                                </p>
                                <a class="btn btn_danger btn_rounded" href="<?php echo base_url('aboutus')?>">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section - End
          ================================================== -->

        <!-- Testimonial Section - Start
          ================================================== -->
        <section class="testimonial_section section_space pb-0">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col col-lg-6">
                        <div class="section_title text-center">
                            <h2 class="sub_title">
                                Testimonials
                                <span class="under_text">Testimonial</span>
                            </h2>
                            <h3 class="title_text mb-0">
                                Some Feedback From Our Happy Clients.
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="testimonial_carousel_3" data-slick='{"arrows": false}'>
                    <div class="common_carousel_3col">
                        <?php
                        if (!empty($reviews)):
                            foreach ($reviews as $review_data):
                                ?>
                        <div class="carousel_item">
                            <div class="testimonial_item_3">
                                <i class="quote_icon flaticon-quotation-right-mark"></i>
                                <div class="hero_wrap">
                                    <div class="hero_image">
                                        <img src="<?php echo base_url(($review_data) ? $review_data->path : FILENOTFOUND); ?>" alt="<?php echo $review_data->username;?>">
                                    </div>
                                    <div class="hero_content">
                                        <h3 class="hero_name"><?php echo $review_data->username;?></h3>
                                        <ul class="reting_star ul_li">
                                            <li class="active"><i class="flaticon-star"></i></li>
                                            <li class="active"><i class="flaticon-star"></i></li>
                                            <li class="active"><i class="flaticon-star"></i></li>
                                            <li class="active"><i class="flaticon-star"></i></li>
                                            <li class="active"><i class="flaticon-star"></i></li>
                                        </ul>
                                    </div>

                                </div>
                                <p>
                                    <?php echo $review_data->review;?>
                                </p>
                                <div class="brand_wrap">
                                    <h4 class="brand_name">
                                        <?php echo date('d-m-Y',strtotime($review_data->datetime));?>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <?php
                            endforeach;
                            endif;
                        ?>
                    </div>
                </div>

            </div>
        </section>
        <!-- Testimonial Section - End
          ================================================== -->

        <!-- FAQ Section - Start
          ================================================== -->
        <section class="faq_section section_space bg_gray">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col col-lg-12">
                        <div class="counter_items_group">
                            <div class="counter_item">
                                <div class="item_icon" data-text-color="#000323">
                                    <i class="flaticon-support"></i>
                                </div>
                                <div class="counter_text"><span class="counter">1200</span>+</div>
                                <h3 class="item_title">Repairing Cases Solved</h3>
                            </div>

                            <div class="counter_item">
                                <div class="item_icon" data-text-color="#000323">
                                    <i class="flaticon-customer-satisfaction"></i>
                                </div>
                                <div class="counter_text"><span class="counter">500</span>+</div>
                                <h3 class="item_title">Happy Customers</h3>
                            </div>

                            <div class="counter_item">
                                <div class="item_icon" data-text-color="#000323">
                                    <i class="flaticon-businessman"></i>
                                </div>
                                <div class="counter_text"><span class="counter">25</span></div>
                                <h3 class="item_title">Experienced Engineers</h3>
                            </div>

                            <div class="counter_item">
                                <div class="item_icon" data-text-color="#000323">
                                    <i class="flaticon-star-1"></i>
                                </div>
                                <div class="counter_text"><span class="counter">99</span>%</div>
                                <h3 class="item_title">Successful Ratings</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- FAQ Section - End
          ================================================== -->

    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
<?php $slider_data = $this->md->select('tbl_banner');
?>
<!-- Zooming Slider
   ============================================= -->
<section id="home-header" class="zooming-slider dark fullheight">
    <div class="bg-transparent fullheight">
        <!-- Slider content -->
        <div class="slider-content">
            <!-- Text Rotater -->
            <ul id="fade">
                <?php
                if (!empty($slider_data)) {
                    foreach ($slider_data as $slider) {
                        ?>
                        <li>
                            <h1><?php echo $slider->title ?></h1>
                        </li>
                    <?php }
                } ?>
            </ul>
            <!-- End Text Rotater -->
            <i class="icon-home-ico"></i>
            <p class="text-uppercase">We Create Sweet Memories</p>
            <a href="<?php echo base_url('about'); ?>" class="btn btn-gold white">DISCOVER MORE</a></div>
        <!-- End Slider content  -->
    </div>
</section>
<!-- End Zooming Slider -->

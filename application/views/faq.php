<?php
echo $page_head;
?>
<body>
<!-- template sections -->
<?php echo $page_header; ?>
<?php echo $page_breadcumb; ?>

<section class="section faq-section" id="faqa">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-7 col-xxl-6">
                <div class="section__header">
                    <span class="section__header-sub-title headingFour wow fadeInDown" data-wow-duration="0.8s"><img
                            src="assets/images/title_vector.png" alt="vector">Frequently Asked Questions</span>
                    <h2 class="section__header-title wow fadeInUp" data-wow-duration="0.8s">Find Answers to Common
                        Questions</h2>
                    <p class="section__header-content wow fadeInDown" data-wow-duration="0.8s">We've compiled a list of
                        frequently asked questions to provide you with quick and helpful answers. If you have a question
                        that is not addressed below</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-between gy-4 gy-lg-0">
            <div class="col-12 col-lg-12 col-xxl-12">
                <div class="accordion wow fadeInDown" data-wow-duration="0.8s" id="faq">
                    <?php
                    if (empty($faq)) :
                        echo "Sorry, content not available";
                    else :
                        foreach ($faq as $key => $faq_data) {
                            ?>
                            <div class="accordion-item <?php echo $key == 0 ? 'accordion_bg' : ''; ?>">
                                <h5 class="accordion-header">
                                    <button class="accordion-button <?php echo $key != 0 ? 'collapsed' : ''; ?>"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#faq-accordion-<?php echo $key + 1; ?>" aria-expanded="true"
                                            aria-controls="faq-accordion-<?php echo $key + 1; ?>">
                                        <?php echo $faq_data->question; ?>
                                    </button>
                                </h5>
                                <div id="faq-accordion-<?php echo $key + 1; ?>"
                                     class="accordion-collapse collapse <?php echo $key == 0 ? 'show' : ''; ?>"
                                     data-bs-parent="#faq">
                                    <div class="accordion-body">
                                        <p class="mb-0">
                                            <?php echo $faq_data->answer; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>
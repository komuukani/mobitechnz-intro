
<?php
echo $page_head;
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

    <!-- Document Wrapper
        ============================================= -->
    <main>
        <?php echo $page_breadcumb; ?>
        <!-- About Section - Start
          ================================================== -->
        <div class="about_section section_space">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 intro_message mt40">
                        <!-- Main Title -->
                        <?php
                        if (empty($policy)) :
                            echo "Sorry, content not available";
                        else :
                            foreach ($policy as $key => $policy_data) {
                                ?>
                                <div class="col-md-12 mb30">
                                    <?php echo $policy_data->policy; ?>
                                </div>
                                <!-- End# Main Title -->
                            <?php }
                        endif;
                        ?>
                    </div>
                    <!-- End intro center -->
                </div>
            </div>
        </div>
        <!-- About Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
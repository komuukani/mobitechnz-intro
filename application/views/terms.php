
<?php
echo $page_head;
?>
<body>
<div class="body_wrap">
    <?php echo $page_header; ?>

    <!-- Document Wrapper
        ============================================= -->
    <main>
        <?php echo $page_breadcumb; ?>
        <!-- About Section - Start
          ================================================== -->
        <div class="about_section section_space">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 intro_message mt40">
                        <?php
                        if (empty($terms)) :
                            echo "Sorry, content not available";
                        else :
                            foreach ($terms as $key => $terms_data) {
                                ?>
                                <div class="col-md-12 mb30">
                                    <?php echo $terms_data->terms; ?>
                                </div>
                                <!-- End# Main Title -->
                            <?php }
                        endif;
                        ?>
                    </div>
                    <!-- End intro center -->
                </div>
            </div>
        </div>
        <!-- About Section - End
          ================================================== -->
    </main>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>


<!-- end of #content -->
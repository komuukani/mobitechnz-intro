<?php
echo $page_head;
$category = $this->md->select('tbl_category');
?>
<body>
<div class="main-wrapper">
    <?php echo $page_header; ?>
    <?php echo $page_breadcumb; ?>
    <div class="edu-event-grid-area edu-section-gap bg-color-white">
        <div class="container">
            <div class="row g-5">

                <!-- Start Event Grid   -->
                <div class="col-12 col-sm-12 col-xl-4 col-md-4" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                    <div class="edu-event event-grid-1 radius-small">
                        <div class="inner">
                            <div class="thumbnail">
                                <a href="<?php echo base_url('search-courses/basic')?>">
                                    <img src="assets/images/event/event-02/event-01.jpg" alt="Event Images">
                                </a>
                            </div>
                            <div class="content">
                                <h5 class="title"><a href="<?php echo base_url('search-courses/basic')?>">Basic</a></h5>
                                <div class="read-more-btn">
                                    <a class="btn-transparent" href="<?php echo base_url('search-courses/basic')?>">Explore Courses<i class="icon-arrow-right-line-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Event Grid   -->

                <!-- Start Event Grid   -->
                <div class="col-12 col-sm-12 col-xl-4 col-md-4" data-sal-delay="200" data-sal="slide-up" data-sal-duration="800">
                    <div class="edu-event event-grid-1 radius-small">
                        <div class="inner">
                            <div class="thumbnail">
                                <a href="<?php echo base_url('search-courses/intermediate')?>">
                                    <img src="assets/images/event/event-02/event-02.jpg" alt="Event Images">
                                </a>
                            </div>
                            <div class="content">
                                <h5 class="title"><a href="<?php echo base_url('search-courses/intermediate')?>">Intermediate</a></h5>
                                <div class="read-more-btn">
                                    <a class="btn-transparent" href="<?php echo base_url('search-courses/intermediate')?>">Explore Courses<i class="icon-arrow-right-line-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Event Grid   -->

                <!-- Start Event Grid   -->
                <div class="col-12 col-sm-12 col-xl-4 col-md-4" data-sal-delay="200" data-sal="slide-up" data-sal-duration="800">
                    <div class="edu-event event-grid-1 radius-small">
                        <div class="inner">
                            <div class="thumbnail">
                                <a href="<?php echo base_url('search-courses/advance')?>">
                                    <img src="assets/images/event/event-02/event-02.jpg" alt="Event Images">
                                </a>
                            </div>
                            <div class="content">
                                <h5 class="title"><a href="<?php echo base_url('search-courses/advance')?>">Advanced</a></h5>
                                <div class="read-more-btn">
                                    <a class="btn-transparent" href="<?php echo base_url('search-courses/advance')?>">Explore Courses<i class="icon-arrow-right-line-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Event Grid   -->


            </div>
        </div>
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>


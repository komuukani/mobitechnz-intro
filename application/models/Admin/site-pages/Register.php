<?php

class Register extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_register';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value

        ## Custom search filter
        $searchFrom = (isset($postData['searchFrom']) ? $postData['searchFrom'] : '');
        $searchTo = (isset($postData['searchTo']) ? $postData['searchTo'] : '');

        ## Search
        $search_arr = array();
        $searchQuery = "";
        if ($searchValue != '') {
            $search_arr[] = " (fname like '%" . $searchValue . "%' or state like '%" . $searchValue . "%' or phone like '%" . $searchValue . "%' or email like '%" . $searchValue . "%' or address like'%" . $searchValue . "%' or entry_date like'%" . $searchValue . "%'  ) ";
        }

        if ($searchFrom != '' && $searchTo != "") {
            if ($searchFrom == $searchTo) {
                $search_arr[] = " register_date like '%" . $searchFrom . "%' ";
            } else {
                $search_arr[] = " register_date BETWEEN '" . $searchFrom . "' AND '" . $searchTo . "' ";
            }
        }

        if (count($search_arr) > 0) {
            $searchQuery = implode(" and ", $search_arr);
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("register_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                "name" => $record->fname,
                "email" => $record->email,
                "phone" => $record->phone,
                "state" => $record->state,
                "city" => $record->city,
                "address" => $record->address,
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->register_date)),
                "delete" => '<a id="deleteItem" data-itemid="' . $record->register_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}

<?php

defined('BASEPATH') or exit('No direct script access allowed');
// Front Side Routes
$route['home'] = 'Pages/index';
$route['contact'] = 'Pages/contact';
$route['reservation'] = 'Pages/reservation';
$route['aboutus'] = 'Pages/aboutus';
$route['gallery'] = 'Pages/gallery';
$route['policy'] = 'Pages/policy';
$route['terms'] = 'Pages/terms';
$route['faq'] = 'Pages/faq';
$route['menu/?(:any)?'] = 'Pages/menu/$2';
$route['blog/?(:any)?'] = 'Pages/blog/$2';
$route['service/?(:any)?'] = 'Pages/service/$2';
$route['404'] = 'Install/page_not_found';


/* ==================================================
  >> ADMIN ROUTES START
  ================================================== */
// >> AUTHENTICATION ROUTES
$route['authorize'] = 'Admin/Login/index';
$route['admin-forgot-password'] = 'Admin/Login/forgot_password';
$route['admin-dashboard'] = 'Admin/Login/dashboard';
$route['admin-profile'] = 'Admin/Login/profile';
$route['admin-setting/?(:any)?'] = 'Admin/Login/setting/$2';
$route['admin-feature-setting'] = 'Admin/Login/feature_setting';
$route['control-panel/?(:any)?'] = 'Admin/Login/control_panel/$3';
$route['admin-logout'] = 'Admin/Login/logout';
// >> COMMON PAGES ROUTES  
$route['manage-contact/?(:any)?'] = 'Admin/Pages/contact/$2';
$route['manage-reservation/?(:any)?'] = 'Admin/Pages/reservation/$2';
$route['manage-email_subscriber/?(:any)?'] = 'Admin/Pages/email/$2';
$route['manage-feedback/?(:any)?'] = 'Admin/Pages/feedback/$2';
$route['manage-banner/?(:any)?/?(:any)?'] = 'Admin/Pages/banner/$2/$3';
$route['manage-gallery/?(:any)?/?(:any)?'] = 'Admin/Pages/gallery/$2/$3';
$route['manage-client/?(:any)?/?(:any)?'] = 'Admin/Pages/client/$2/$3';
$route['manage-aboutus/?(:any)?/?(:any)?'] = 'Admin/Pages/aboutus/$2/$3';
$route['manage-policy/?(:any)?/?(:any)?'] = 'Admin/Pages/policy/$2/$3';
$route['manage-terms/?(:any)?/?(:any)?'] = 'Admin/Pages/terms/$2/$3';
$route['manage-inquiry/?(:any)?/?(:any)?'] = 'Admin/Pages/inquiry/$2/$3';
$route['manage-review/?(:any)?/?(:any)?'] = 'Admin/Pages/review/$2/$3';
$route['manage-blog/?(:any)?/?(:any)?'] = 'Admin/Pages/blog/$2/$3';
$route['manage-team/?(:any)?/?(:any)?'] = 'Admin/Pages/team/$2/$3';
$route['manage-video/?(:any)?/?(:any)?'] = 'Admin/Pages/video/$2/$3';
$route['manage-seo/?(:any)?/?(:any)?'] = 'Admin/Pages/seo/$2/$3';
$route['manage-category/?(:any)?/?(:any)?'] = 'Admin/Pages/category/$2/$3';
$route['manage-subcategory/?(:any)?/?(:any)?'] = 'Admin/Pages/subcategory/$2/$3';
$route['manage-user/?(:any)?/?(:any)?'] = 'Admin/Pages/user/$2/$3';
$route['manage-role/?(:any)?/?(:any)?'] = 'Admin/Pages/role/$2/$3';
$route['manage-permission/?(:any)?/?(:any)?'] = 'Admin/Pages/permission/$2/$3';
$route['manage-mainmenu/?(:any)?/?(:any)?'] = 'Admin/Pages/mainmenu/$2/$3';
$route['manage-submenu/?(:any)?/?(:any)?'] = 'Admin/Pages/submenu/$2/$3';
// >> REMOVE ROUTES
$route['remove/(:any)/(:any)/?(:any)?'] = 'Admin/Pages/delete/$2/$3/$4';
/* XXXXXXXXXXXXXXXX     << ADMIN ROUTES END    XXXXXXXXXXXXXXXX */

/* ==================================================
  >> DEFAULT ROUTES START
  ================================================== */
$route['404_override'] = 'Install/page_not_found';
$route['translate_uri_dashes'] = FALSE;
$route['default_controller'] = 'Pages';
/* XXXXXXXXXXXXXXXX     << DEFAULT ROUTES END    XXXXXXXXXXXXXXXX */


/* ==================================================
  >> MAIN MENU & SUB MENU FROM DATABASE START
  ================================================== */
require_once(BASEPATH . 'database/DB.php');
$db = &DB();
// Main Menu 
if (INSTALLER_SETTING['database']) :
    if ($db->table_exists('tbl_mainmenu')) :
        $query = $db->get('tbl_mainmenu');  // get all main menu
        $result = $query->result();
        foreach ($result as $row) :
            $route[$row->slug] = $row->url; // set dynamic main menu route
        endforeach;
        // Sub Menu
        $query = $db->get('tbl_submenu');   // get all sub menu
        $result = $query->result();
        foreach ($result as $row) :
            $route[$row->slug] = $row->url; // set dynamic main sub route
        endforeach;
    endif;
endif;
///* XXXXXXXXXXX   << MAIN MENU & SUB MENU FROM DATABASE ROUTES END  XXXXXXXXXX */
